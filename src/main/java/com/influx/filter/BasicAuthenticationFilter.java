package com.influx.filter;

/**
 * Created by chris on 11/19/14.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Base64;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.influx.service.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *
 * @author chris
 */
public class BasicAuthenticationFilter extends OncePerRequestFilter
{
    @Autowired
    AuthenticationService authService;

    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws ServletException, IOException
    {
        String service = (req.getPathInfo() != null) ? req.getPathInfo().replaceFirst("/", "") : "";

        // Adding Order Submit Api's for external access
        if(service != null && service.contains("external")) {
        	service = "ExternalService";
        }
        try {
            // Get Authorization header
            String authorization = req.getHeader("Authorization");

            if (!isAuthorized(authorization, service)) {
                res.setHeader("WWW-Authenticate", "BASIC realm=\"influx\"");
                res.setStatus(HttpServletResponse.SC_UNAUTHORIZED);

                PrintWriter out = res.getWriter();
                out.println("<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">");
                out.println("   <S:Body>");
                out.println("      <S:Fault xmlns:ns4=\"http://www.w3.org/2003/05/soap-envelope\">");
                out.println("         <faultcode>S:Server</faultcode>");
                out.println("         <faultstring>Authentication failed!</faultstring>");
                out.println("      </S:Fault>");
                out.println("   </S:Body>");
                out.println("</S:Envelope>");

            }else{
                chain.doFilter(req, res);
            }
        }catch(Exception ex) {
            res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);

            PrintWriter out = res.getWriter();
            out.println("<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">");
            out.println("   <S:Body>");
            out.println("      <S:Fault xmlns:ns4=\"http://www.w3.org/2003/05/soap-envelope\">");
            out.println("         <faultcode>S:Server</faultcode>");
            out.println("         <faultstring>" + ex + "</faultstring>");
            out.println("      </S:Fault>");
            out.println("   </S:Body>");
            out.println("</S:Envelope>");
        }
    }

    protected boolean isAuthorized(String authorization, String service) throws Exception
    {
        if (authorization == null) {
            return false;  // no auth
        }
        if (!authorization.toUpperCase().startsWith("BASIC ")) {
            return false;  // we only do BASIC
        }

        //Get encoded user and password, comes after "BASIC "
        //Decode it, using any base 64 decoder
        byte[] decodedBytes = Base64.getDecoder().decode(authorization.substring(6));
        String credentials = new String(decodedBytes);
 
      //  BASE64Decoder dec = new sun.misc.BASE64Decoder();
     //   String credentials = new String(dec.decodeBuffer(authorization.substring(6)));

        String username = "";
        String password = "";

        int p = credentials.indexOf(":");
        if (p > -1) {
            username = credentials.substring(0, p);
            password = credentials.substring(p+1);
        } else {
            throw new RuntimeException("There was an error while decoding the Authentication!");
        }

        //check for authorization
        if(authService.isAuthorized(username, password, service)) {
            return true;
        } else {
            return false;
        }
    }
}