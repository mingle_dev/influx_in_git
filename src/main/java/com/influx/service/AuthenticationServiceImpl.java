package com.influx.service;

/**
 * Created by chris on 11/19/14.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.influx.bean.Service;
import com.influx.bean.User;
import java.util.List;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

/**
 *
 * @author chris
 */
@Repository
public class AuthenticationServiceImpl implements AuthenticationService
{
    @Autowired
    private MongoTemplate mongoTemplate;

    public static final String COLLECTION_NAME = "user_authorization";
    private static Logger log = Logger.getLogger( AuthenticationServiceImpl.class );

    @Override
    public boolean isAuthorized(String username, String password, String service)
    {
        Query query = new Query();

        if(service != null && !service.isEmpty()) {
            query.addCriteria( Criteria.where("services").in( (Object) service ) );
            //query.addCriteria( Criteria.where("services").in( new String[] {service} ) );
        }

        query.addCriteria( Criteria.where("username").is(username) );
        query.addCriteria( Criteria.where("password").is(password) );

        //List<User> list = mongoTemplate.find(query, User.class, COLLECTION_NAME);

        long cnt = mongoTemplate.count(query, COLLECTION_NAME);

        return (cnt == 1) ? true : false;
    }

    @Override
    public String addUser(String username, String password, List<Service> services)
    {
        String uuid = UUID.randomUUID().toString();

        User u = new User();
        u.setId(uuid);
        u.setUsername(username);
        u.setPassword(password);
        u.setServices(services);

        mongoTemplate.insert(u, COLLECTION_NAME);

        return uuid;

    }
//        DBCollection table = mongoDb.getCollection(COLLECTION_NAME);
//
//        BasicDBObject query = new BasicDBObject();
//	query.put("username", username);
//        query.put("password", password);
//        query.put("services", new BasicDBObject("$in", new String[] {service}));
//
//	DBCursor cursor = table.find(query);
//
//        return (cursor.count() == 1) ? true : false;
//    }
//
//    public String add(String username, String password, String service)
//    {
//        String uuid = UUID.randomUUID().toString();
//
//        DBCollection table = mongoDb.getCollection(COLLECTION_NAME);
//
//	BasicDBObject document = new BasicDBObject();
//	document.put("id", uuid);
//	document.put("username", username);
//	document.put("password", password);
//        document.put("service", service);
//	table.insert(document);
//
//        return uuid;
//    }
}