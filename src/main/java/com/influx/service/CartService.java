package com.influx.service;

import com.influx.bean.SyncCart;

/**
 * Created by chris on 11/10/14.
 */
public interface CartService
{
    public SyncCart getCart(String userId);
    public String createCart(SyncCart items);
    public String importCart(String username, String tokenId) throws Exception;
}
