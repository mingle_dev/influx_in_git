package com.influx.service;

import com.influx.bean.Service;
import java.util.List;

/**
 * Created by chris on 11/19/14.
 */
public interface AuthenticationService
{
    public boolean isAuthorized(String username, String password, String module);
    public String addUser(String username, String password, List<Service> services);
}
