package com.influx.service;

import com.influx.bean.SyncCart;
import com.influx.bean.SyncCartItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

/**
 * Created by chris on 11/10/14.
 */
@Service
@Transactional
public class CartServiceImpl implements CartService
{
    @Autowired
    private MongoTemplate mongoTemplate;

    public static final String COLLECTION_NAME = "external_cart";

    @Override
    public SyncCart getCart(String id)
    {
        return mongoTemplate.findById(id, SyncCart.class, COLLECTION_NAME);
    }

    @Override
    public String createCart(SyncCart cart)
    {
        String uuid = UUID.randomUUID().toString();

        if(!mongoTemplate.collectionExists(COLLECTION_NAME))
            mongoTemplate.createCollection(COLLECTION_NAME);

        cart.setId(uuid);

        mongoTemplate.insert(cart, COLLECTION_NAME);

        return uuid;
    }

  /*  @Override
    public String importCart(String userId, String tokenId) throws Exception
    {
        SyncCart cart = getCart(tokenId);
        for(SyncCartItem item : cart.getItems())
        {
            //verify that item is valid in icsp

            //add to shopping cart in portal
            //(String userId, String productCode, String note, int qty, boolean overrideQty, BigDecimal price);
            //cartDao.saveItem(userId, item.getItemName(), "", item.getQuantity(), false, null);
        }

        return "";
    } */
    
    @Override
    public String importCart(String userId, String tokenId) throws Exception
	{
	  	String returnString = "<items>";
	    SyncCart cart = getCart(tokenId);
	
	    for (SyncCartItem item : cart.getItems())
	    {
	    	System.out.println("cart item: " + item.getItemName() + " " + "quantity: " + item.getQuantity());
	    	returnString = returnString + "<item><itemName>" + item.getItemName() + "</itemName><quantity>" + item.getQuantity() + "</quantity></item>";
	    }

	    returnString = returnString + "</items>";
	    return returnString;
	  }


    /* jndi testing
    @Autowired
    DataSource dataSource;

    public void getCart(String userId)
    {
        DBI dbi = new DBI(dataSource);

        //JDBCConnectionPool ds = JdbcConnectionPool.create("jdbc:h2:mem:test2", "username", "password");

        //DBI dbi = new DBI(ds);

        CartDao dao = dbi.open(CartDao.class);

        String id = dao.findById(userId);

        System.out.println("id found: " + id);

    }
    */
}
