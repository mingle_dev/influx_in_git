package com.influx.ws;

import com.infor.ion.workflowengine.externallogic.Variable;
import com.infor.ion.workflowengine.externallogic.WorkflowInfo;

import javax.jws.WebService;
import java.util.List;

@WebService
public interface ExecuterSoapService
{
    public List<Variable> execute(WorkflowInfo workflowInfo, String command, List<Variable> inputVariables);
}

/*
    username: "3AZg9MxAnzJARsJ",
    password: "WXJJW99NAaeRkw4DzafaLknZCJaPLKAbNbLm",
 */