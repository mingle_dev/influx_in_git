/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.ws;

import com.influx.bean.SyncCart;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author chris
 */
@WebService
public interface OrderSoapService
{
    @WebMethod(operationName = "createExternalCart")
    public @WebResult(name = "tokenId") String createExternalCart(@WebParam(name = "items") @XmlElement(required=true, nillable=false) SyncCart items);

    @WebMethod(operationName = "importExternalCart")
    public @WebResult(name = "result") String importExternalCart(@WebParam(name = "username") @XmlElement(required=true, nillable=false) String username,
                                                                 @WebParam(name = "tokenId") @XmlElement(required=true, nillable=false) String tokenId)
            throws Exception;
}