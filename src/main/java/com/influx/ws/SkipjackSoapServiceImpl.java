package com.influx.ws;

import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jws.WebService;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.influx.ConfigReader;
import com.influx.bean.Customer;
import com.influx.bean.LineItem;
import com.influx.bean.LineItems;
import com.influx.bean.PurchaseData;
import com.influx.bean.cc.CardReceipt;
import com.influx.bean.cc.Company;
import com.influx.bean.cc.ReceiptReport;
import com.influx.bean.sapModel.SalesOrderDetails;
import com.influx.bean.sapModel.SalesOrderItem;
import com.influx.bean.sapModel.SalesOrderRoot;

@WebService(serviceName = "SkipjackService",
            portName = "SkipjackServiceSoapBinding",
            endpointInterface = "com.influx.ws.SkipjackSoapService")
public class SkipjackSoapServiceImpl implements SkipjackSoapService
{
    private List<CardReceipt> receipts = null;
    private List<Company> companies = null;
    private static Logger log = Logger.getLogger(SkipjackSoapServiceImpl.class);
    //public static final String version = "1.13.03.18";
    public static final String version1 = ConfigReader.getProperty("ff1.0.version"); //"1.13.03.18";
    public static final String version2 = ConfigReader.getProperty("ff2.0.version"); //"1.14.0";
    public static final String version3 = ConfigReader.getProperty("ff3.0.version"); //"1.15.0";
    private static final int CONO = ConfigReader.getPropertyAsInt("sxe.cono");
    private static final String OPER = ConfigReader.getProperty("sxe.oper");
    private static final String TRX_AES_IV = ConfigReader.getProperty("trx.iv");
    private static final String TRX_AES_KEY = ConfigReader.getProperty("trx.key");
    private static final String TRX_HOST = ConfigReader.getProperty("trx.host");
    private static final String TRX_PORT = ConfigReader.getProperty("trx.port");
    private static final String FUND_APP_CONO = "fund.app.cono";
    private static final String FUND_APP_CLIENTID_SUFFIX = "clientId";
    private static final String FUND_APP_CLIENTIDS_SUFFIX = "clientIds";
    private static HashMap<String,String> responseCodes = new HashMap<String, String>();

    //
    private static final String STORE_INFO = "fund.app.storeId";
    
    public Map<String,String> clientIds = null; //put here for now...
    
    public static final String sapHost = ConfigReader.getProperty("sap.hostOrder");
    public static final String sapUser = ConfigReader.getProperty("sap.user");
    public static final String sapPass = ConfigReader.getProperty("sap.pass");
    
    public static final String dbDriver = ConfigReader.getProperty("SQL.DRIVER");
	public static final String dbUrl = ConfigReader.getProperty("SQL.URL");
	public static final String dbUser = ConfigReader.getProperty("SQL.USER");
	public static final String dbPass = ConfigReader.getProperty("SQL.PASS");

    static {

     

    }

    private class DateValidator {

        public boolean isThisDateValid(String dateToValidate, String dateFromat){

            if(dateToValidate == null){
                return false;
            }

            SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
            sdf.setLenient(false);

            try {

                //if not valid, it will throw ParseException
                Date date = sdf.parse(dateToValidate);
                System.out.println(date);

            } catch (ParseException e) {

                e.printStackTrace();
                return false;
            }

            return true;
        }

    }

    public SkipjackSoapServiceImpl()
    {
        receipts = new ArrayList<CardReceipt>();
        clientIds = new HashMap<String,String>();
        companies = new ArrayList<Company>();
        //get the company numbers supported
        String conos = ConfigReader.getProperty(FUND_APP_CONO);

        StringTokenizer t1 = new StringTokenizer (conos, ",");

        while (t1.hasMoreElements())
        {
            //get the list of clientIds for each company
            String cono = t1.nextToken();
            Company company = new Company();
            company.setCono(Integer.parseInt(cono));
            String prop_prefix = FUND_APP_CONO + "." + cono + ".";
            String clientId_prop = prop_prefix + FUND_APP_CLIENTIDS_SUFFIX;
            String ids = ConfigReader.getProperty (clientId_prop);
            //parse the list of clientIds defined in properties delimited by ,
            StringTokenizer t2 = new StringTokenizer(ids, ",");

            List<String> whses = new ArrayList<String>();

            while (t2.hasMoreElements())
            {
                String clientId = (String)t2.nextElement();
                String property = prop_prefix + FUND_APP_CLIENTID_SUFFIX + "." + clientId;
                String value = ConfigReader.getProperty(property);
                clientIds.put(clientId, value);
                whses.add(clientId);
            }

            company.setWhses(whses);
            companies.add(company);
        }

    }

    @Override
    public String getVersion() throws Exception
    {
        return version1;
    }
    
    @Override
    public String getFF2Version() throws Exception
    {
        return version3;
    }

    @Override
    public List<Company> getCompanies() throws Exception
    {
        return companies;
    }
    
    @Override
    public String getWhseAddress(String whseNo) throws Exception
    {
    	String address = "";
    	String prop_key = STORE_INFO + "." + whseNo;
    	address = ConfigReader.getProperty (prop_key);
        return address;
    }

   
    public static CloseableHttpClient getCloseableHttpClient()
    {
    	CloseableHttpClient httpClient = null;
    	try {
    		httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
    		        .setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy()
    		        {
    		            public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException
    		            {
    		                return true;
    		            }

						@Override
						public boolean isTrusted(java.security.cert.X509Certificate[] chain, String authType)
								throws java.security.cert.CertificateException {
							// TODO Auto-generated method stub
							return true;
						}
    		        }).build()).build();

    	} catch (KeyManagementException e) {
    		 log.info("KeyManagementException in creating http client instance", e);
    	} catch (NoSuchAlgorithmException e) {
    		 log.info("NoSuchAlgorithmException in creating http client instance", e);
    	} catch (KeyStoreException e) {
    		 log.info("KeyStoreException in creating http client instance", e);
    	}
    	return httpClient;
    }
    
    
    public String addLeadingZeros(String value, int totalDigits) {
		if(value.isEmpty() || !StringUtils.isNumeric(value)) {
			return value;
		}
		String formatted = String.format("%0" + totalDigits + "d", Integer.parseInt(value));
		return formatted;
	}
    
   
   @Override
   public PurchaseData getOrderDetails(String orderNo) throws Exception
   {

	   System.out.println("Fetching Order details for :"+orderNo);
	   log.info("Fetching Order details for :"+orderNo);
	   
	   PurchaseData purchaseData = new PurchaseData(); 
       //level 3
        try {
        	
        	if(orderNo != null) {
        		orderNo = addLeadingZeros(orderNo, 10);
        	}
        	CloseableHttpClient client = getCloseableHttpClient();
        	ObjectMapper mapper = new ObjectMapper();
        	mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        	
			HttpPost post = new HttpPost(sapHost);
			String authInput = sapUser + ":" + sapPass ;
			String encodedString = Base64.getEncoder().encodeToString(authInput.getBytes());
			 
		    post.setHeader("Accept", "application/json");
		    post.setHeader("Content-type", "application/json");
		    post.setHeader("Authorization", "Basic "+encodedString);
        	
		    ObjectNode mainNode = mapper.createObjectNode();
			ObjectNode reqNode = mapper.createObjectNode();
			ObjectNode rootNode = mapper.createObjectNode();

		    rootNode.put("SalesOrderNumber", orderNo);
			reqNode.set("SalesOrderNumber", rootNode);
			mainNode.set("Request", reqNode);
        	
		    String jsonString = mapper.writeValueAsString(mainNode);
			StringEntity reqEntity = new StringEntity(jsonString);
		    post.setEntity(reqEntity);
			
		    CloseableHttpResponse response = client.execute(post);
			if(response.getStatusLine().getStatusCode() == 200) {
				HttpEntity respEntity = response.getEntity();
			    if (respEntity != null) {
			    	String result = EntityUtils.toString(respEntity);
			    	
			    	ObjectMapper mapper1 = new ObjectMapper();
			    			mapper1.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);
			    	
			    	SalesOrderRoot root = mapper1.readValue(result, SalesOrderRoot.class);

			    	if(root != null && root.getSALESORDERDETAILSSet() != null && root.getSALESORDERDETAILSSet().getSalesOrderDetails() != null) {
			    		SalesOrderDetails order =  root.getSALESORDERDETAILSSet().getSalesOrderDetails();
			    		
			    		purchaseData.setLocalTaxAmount("0.00");
			        	purchaseData.setNationalTaxAmount("0.00");
			    		purchaseData.setCustomerCode(order.getCustomer());
			            purchaseData.setSummaryCommodityCode("1711");
			            purchaseData.setFreightAmount("0.00");
			            purchaseData.setDutyAmount("0.00");
			            purchaseData.setDestinationPostal("30092");
			            purchaseData.setShipFromPostal("30041");
			            purchaseData.setDestinationCountry("USA");
			            purchaseData.setvATInvoiceReferenceId("123456");
			            purchaseData.setvATTaxAmount("0.00");
			            purchaseData.setvATTaxRate("0.00");
			            purchaseData.setTaxExempt("1");
			             
			             if(order.getSALESORDERITEMSet() != null && order.getSALESORDERITEMSet().getSalesOrderItems() != null 
			            		 && order.getSALESORDERITEMSet().getSalesOrderItems().size() > 0) {
			            	 LineItem lineItem;
			            	 List<LineItem> lineItems = new ArrayList<LineItem>(); 
			  	           	 int l = (order.getSALESORDERITEMSet().getSalesOrderItems().size() > 10) ? 10 : order.getSALESORDERITEMSet().getSalesOrderItems().size();
			  	           	 
			  	           	 for(int i=0; i<l; i++) {
			  	           		 	SalesOrderItem sol = order.getSALESORDERITEMSet().getSalesOrderItems().get(i);
			  	           	 		lineItem = new LineItem();
				  	           	 	lineItem.setCommodityCode(sol.getPlant());
				  	           	 	if(formatInputQty(sol.getReqQty()).equalsIgnoreCase("0")) {
				  	           	 	lineItem.setQuantity("1");
				  	           	 	} else {
				  	           	 		lineItem.setQuantity(formatInputQty(sol.getReqQty()));
				  	           	 	}
				  	           	 	
				  	           	 	lineItem.setUnitOfMeasure("EA");
				  	           	 	lineItem.setUnitAmount(formatInput(sol.getNetPrice()));
				  	           	 	lineItem.setTaxAmount("0.00");
				  	           	 	lineItem.setTaxRate("0.00");
				  	           	 	lineItem.setDiscountAmount("0.00");
				  	           	 	lineItem.setDescription(order.getVbeln());
				  	
				  	           	 	if(sol.getMaterial().length() <= 12)
				  	           	 		lineItem.setProductCode(StringEscapeUtils.escapeXml(sol.getMaterial()));
				  	           	 	else
				  	           	 		lineItem.setProductCode(StringEscapeUtils.escapeXml(sol.getMaterial().substring(1, 12)));
				  	           	lineItems.add(lineItem);
			  	           	 }
			  	           	 LineItems lil = new LineItems();
			  	           	 lil.setLineItem(lineItems);
			  	           	 purchaseData.setLineItems(lil);
			            }
			    	}
			    }
			}
       }catch(Exception ex2) {
           log.error(ex2.getMessage());
       }
        
       return purchaseData;
   }
  
	public String formatInput(String input) {
		   
		   String s = "1.00";
		   try {
			   input = input.trim();
			   
			   if(input != null && input.length() > 0) {
				   s = input.substring(0, input.lastIndexOf("."));
				   if(s.equalsIgnoreCase("0")) {
					   input = "1.00";
				   }
			   } 
			   
			   Float inputF =Float.parseFloat(input);
			   DecimalFormat df = new DecimalFormat("0.00");
			   df.setMaximumFractionDigits(2);
			   s = df.format(inputF);
		
		   } catch (NumberFormatException e) {
			   e.printStackTrace();
			   s = "1.00";
		   } 
		   return s;
	   }
   
   public String formatInputQty(String input) {
	   String s = "1";
	   if(input != null && input.length() > 0) {
		   s = input.substring(0, input.lastIndexOf("."));
	   } 
	   return s;
   }

   @Override
   public List<CardReceipt> getReceipts() throws Exception
   {
       //return this.receipts;
	   return getDBReceipts();
   }
   
   
    /**
     *
     * @param dirtyXml
     * @return
     */
    private String cleanXml(String dirtyXml)
    {
        String cleanXml = null;
        Pattern pattern = null;
        Matcher matcher = null;
        pattern = Pattern.compile("[\\000]*");
        matcher = pattern.matcher(dirtyXml);
        if (matcher.find()) {
            cleanXml = matcher.replaceAll("");
        }

        return cleanXml;
    }

    /**
     * Encodes a string
     * @param str
     * @return
     */
    private String encodeUrl(String str)
    {
        String results = "";
        try {
            str = str.trim();
            results = URLEncoder.encode(str, "UTF-8");
        }
        catch(Exception ex) {}

        return results;
    }

    /**
     * Encodes and integer
     * @param i
     * @return
     */
    @SuppressWarnings("unused")
    private String encodeUrl(int i)
    {
        String results = "";
        try {
            results = URLEncoder.encode(String.valueOf(i), "UTF-8");
        }
        catch(Exception ex) {}

        return results;
    }



    private void addReceipt(CardReceipt receipt)
    {
        //delete any receipts from the previous day
        //this isn't efficient and will be changed if
        //we get continous expected results
        DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
        Date date = new Date();
        String d = dateFormat.format(date);

        for(int i=0; i<receipts.size(); i++)
        {
            CardReceipt r = receipts.get(i);
            if( !d.equals(r.getTransDate()) )
                receipts.remove(i);
        }

        //add the receipt
        receipts.add(receipt);
    }
    
    /**
    *
    * @param request CardRequest
    * @return String
    * @throws Exception
    */
   @Override
   public String postReceipt(CardReceipt receipt)
    {
	   try {
		   log.info("posting Receipt0 :"+receipt.getAuthCode());
		   DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		   Date date = new Date();
	       receipt.setTransDate(dateFormat.format(date));
	       
	       String address = getWhseAddress(receipt.getWarehouse());
	       receipt.setWhseAddress(address); 
	       
		   //addReceipt(receipt);
	       postDBReceipt(receipt);
	       
		   return "success";
	   }catch (Exception e){
           e.printStackTrace();
           return "failed";
       }
    }
   
   
   public void postDBReceipt(CardReceipt r)
   {
	   String sql = "INSERT INTO [dbo].[FFReceipts]\r\n" + 
		   		"           ([CompanyNumber]\r\n" + 
		   		"           ,[Warehouse]\r\n" + 
		   		"           ,[OrderNo]\r\n" + 
		   		"           ,[CardName]\r\n" + 
		   		"           ,[CardNumber]\r\n" + 
		   		"           ,[CardType]\r\n" + 
		   		"           ,[TransType]\r\n" + 
		   		"           ,[TransAmount]\r\n" + 
		   		"           ,[TransId]\r\n" + 
		   		"           ,[AuthCode]\r\n" + 
		   		"           ,[ResponseCode]\r\n" + 
		   		"           ,[ResponseMessage]\r\n" + 
		   		"           ,[WhseAddress]\r\n" + 
		   		"           ,[TransDate])\r\n" + 
		   		"     VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";   
	   Connection conn = null;
		
		try {

			DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(dbDriver);
	        dataSource.setUrl(dbUrl);
	        dataSource.setUsername(dbUser);
	        dataSource.setPassword(dbPass);
	        
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, r.getCompanyNumber());
			ps.setString(2, r.getWarehouse());
			ps.setString(3, r.getOrderNo());
			ps.setString(4, r.getCardName());
			ps.setString(5, r.getCardNumber());
			ps.setString(6, r.getCardType());
			ps.setString(7, r.getTransType());
			ps.setString(8, r.getTransAmount());
			ps.setString(9, r.getTransId());
			ps.setString(10, r.getAuthCode());
			ps.setString(11, r.getResponseCode());
			ps.setString(12, r.getResponseMessage());
			ps.setString(13, r.getWhseAddress());
			ps.setString(14, r.getTransDate());
			ps.executeUpdate();

			ps.close();

		} catch (SQLException e) {
			 e.printStackTrace();

		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
   }
   
   public String checkInputValue(String input) {
	   if(input != null) {
		   return input; 
	   } 
	   return "";
   }
   
   
   public List<CardReceipt> getDBReceipts()
   {
	   
	   String sql = "SELECT * FROM FFReceipts where TransDate = ?";
	   List<CardReceipt> receiptList = new ArrayList<CardReceipt>();
	   Connection conn = null;
		
		try {

			SimpleDateFormat sdf = new SimpleDateFormat("yyMMdd");
			String today = sdf.format(new Date());
			
			DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(dbDriver);
	        dataSource.setUrl(dbUrl);
	        dataSource.setUsername(dbUser);
	        dataSource.setPassword(dbPass);
	        
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, today);

			CardReceipt cardReceipt = null;
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				cardReceipt = new CardReceipt(
						rs.getString("CompanyNumber"),
						rs.getString("Warehouse"),
						rs.getString("OrderNo"),
						rs.getString("CardName"),
						rs.getString("CardNumber"),
						rs.getString("CardType"),
						rs.getString("CardExpMonth"),
						rs.getString("CardExpYear"),
						rs.getString("CardExpDate"),
						rs.getString("TransType"),
						rs.getString("TransAmount"),
						rs.getString("BillAddress"),
						rs.getString("BillCity"),
						rs.getString("BillState"),
						rs.getString("BillZip"),
						rs.getString("Phone"),
						rs.getString("Email"),
						rs.getString("TransId"),
						rs.getString("AuthCode"),
						rs.getString("ResponseCode"),
						rs.getString("ResponseMessage"),
						rs.getString("WhseAddress"),
						rs.getString("TransDate")
						
				);
				receiptList.add(cardReceipt);
			}
			rs.close();
			ps.close();
			
		} catch (SQLException e) {
			 e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
		return receiptList;
   }

    /**
     *
     * @param responseCode
     * @return
     */
    private String lookupResponseCode(String responseCode)
    {
        String message = responseCodes.get(responseCode);

        if (message == null || message.equals(""))

            message = "Declined: Unknown Reason. Contact Card Issuer\n";

        return message;
    }


    public static void main (String[] args)
    {
       

    }

    

}
