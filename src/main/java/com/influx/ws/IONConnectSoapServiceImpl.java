package com.influx.ws;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.jws.WebService;

import com.influx.ConfigReader;
import com.influx.bean.Order;
import com.influx.bean.OrderType;
import com.influx.dataobject.sx.AccountDao;
import com.influx.dataobject.sx.ReportDao;
import org.apache.log4j.Logger;

/**
 *
 * @author chris.weaver
 * 
 * This service is to be used only by the ION Connector
 * 
 */
@WebService(serviceName = "IONConnectService",
            portName = "IONConnectServiceSoapBinding",
            endpointInterface = "com.influx.ws.IONConnectSoapService")
public class IONConnectSoapServiceImpl implements IONConnectSoapService
{
    private static Logger log = Logger.getLogger( IONConnectSoapServiceImpl.class );
    private static final int CONO = ConfigReader.getPropertyAsInt("sxe.cono");
    private static final String OPER = ConfigReader.getProperty("sxe.oper");

    private List<String> ignoreShipViaList;
    
    public IONConnectSoapServiceImpl()
    {
        ignoreShipViaList = Collections.synchronizedList(new ArrayList<String>());
        ignoreShipViaList.add("PKP");
        ignoreShipViaList.add("DONT");
        
        log.info("IONConnectService() Initalized");
    }

    @Override
    public String notifyShipment(String conoStr, String orderno, String shipvia)
    {
        AccountDao accountDao = new AccountDao();
        ReportDao reportDao = new ReportDao();
        
        log.debug("Bill of Lading arrived; cono:" + conoStr + " order:" + orderno + " shipvia:" + shipvia);
        
        try {
            String _type = orderno.substring(0, 2);
            String[] _orderdata = orderno.substring(2).split("-");

            int cono = Integer.parseInt(conoStr);
            int ordno = Integer.parseInt(_orderdata[0]);
            int ordsuf = Integer.parseInt(_orderdata[1]);
            
            
            //fetch order info not in Shipment BOD
            Order order = null;

            if(_type.equalsIgnoreCase("OE"))
                order = accountDao.getOrder(cono, OPER, OrderType.OE, ordno, ordsuf);
            else if(_type.equalsIgnoreCase("WT"))
                order = accountDao.getOrder(cono, OPER, OrderType.WT, ordno, ordsuf);
            else
                return "Unknown BOD Type: " + orderno;

            //try again until we get this fixed...
            if(order == null) {
                log.error("Failed to collect order data from SXAPI");
            }
            
            if(order.getShipVia() == null)
                order.setShipVia("");

            //schedule a OEEPL or WTEPL to print if the following conditions are true:
            // stagecd = 3
            // shipviaty not in ignore list
            if(!ignoreShipViaList.contains(order.getShipVia().toUpperCase()))
            {
                if(order.getStageCode() == 3 ||
                   (order.getStageCode() == 6 && order.getTransType().equalsIgnoreCase("DO")))
                {
                    log.info("Bill of Lading submit for print; cono:" + cono + " whse:" + order.getWhse() + " order:" + orderno + " shipvia:" + shipvia);
                    reportDao.printBOL(cono, OPER, order.getOrderType(), order.getOrderNo(), order.getWhse());
                }
            }
            
        }catch(Exception ex) {
            log.error("notifyShipment " + ex);
        }
        
        return "Got you bod: " + conoStr + " : " + orderno;
    }
}