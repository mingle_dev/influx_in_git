package com.influx.ws;

import com.influx.bean.Product;
import com.influx.bean.Products;

import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;
import java.math.BigDecimal;
import java.util.List;

@WebService
public interface CatalogSoapService
{
    public @WebResult(name = "product") Product getItemPrice(
                    @WebParam(name = "cono") @XmlElement(required=true, nillable=false) int cono,
                    @WebParam(name = "custno") @XmlElement(required=true, nillable=false) String custno,
                    @WebParam(name = "shipto") String shipto,
                    @WebParam(name = "whse") String whse,
                    @WebParam(name = "prod") @XmlElement(required=true, nillable=false) String prod);

    public @WebResult(name = "products") Products getItemPriceMultiple(
                    @WebParam(name = "cono") @XmlElement(required=true, nillable=false) int cono,
                    @WebParam(name = "custno") @XmlElement(required=true, nillable=false) String custno,
                    @WebParam(name = "shipto") String shipto,
                    @WebParam(name = "whse") String whse,
                    @WebParam(name = "prod") @XmlElement(required=true, nillable=false) String prod[]);
}
