package com.influx.ws;

import java.util.List;
import java.util.Map;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;

import com.influx.bean.PurchaseData;
import com.influx.bean.cc.CardReceipt;
import com.influx.bean.cc.CardRequest;
import com.influx.bean.cc.Company;
import com.influx.bean.cc.ReceiptReport;
import com.influx.bean.cc.SkipjackResult;

@WebService
public interface SkipjackSoapService
{
    @WebMethod(operationName = "getVersion")
    public @WebResult(name="version") String getVersion() throws Exception;

    @WebMethod(operationName = "getCompanies")
    public @WebResult(name="companyList") List<Company> getCompanies() throws Exception;

    @WebMethod(operationName = "getReceipts")
    public @WebResult(name="cardReceipt") List<CardReceipt> getReceipts() throws Exception;
    
    @WebMethod(operationName = "getOrderDetails")
    public @WebResult(name="purchaseData") PurchaseData getOrderDetails(@WebParam(name="orderno") String OrderNo) throws Exception;
        
    @WebMethod(operationName = "postReceipt")
    public @WebResult(name="result") String postReceipt(@WebParam(name="cardReceipt") CardReceipt request) throws Exception;
        
    @WebMethod(operationName = "getFF2Version")
    public @WebResult(name="version") String getFF2Version() throws Exception;
    
    @WebMethod(operationName = "getWhseAddress")
    public @WebResult(name="whseAddress") String getWhseAddress(@WebParam(name="whseno") String whseNo) throws Exception;
    
 
}
