package com.influx.ws;

import com.influx.ConfigReader;
import com.influx.bean.Product;
import com.influx.dataobject.sx.ProductDao;
import com.infor.ion.workflowengine.externallogic.*;
import org.apache.log4j.Logger;

import javax.jws.WebService;
import javax.xml.datatype.XMLGregorianCalendar;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@WebService(serviceName = "ExternalLogicExecuter",
            portName = "ExternalLogicExecuterSoapBinding",
            targetNamespace = "http://www.infor.com/ion/workflowengine/externallogic",
            endpointInterface = "com.infor.ion.workflowengine.externallogic.IExternalLogicService")
public class ExecuterSoapServiceImpl implements ExecuterSoapService
{
    private static Logger log = Logger.getLogger(IONConnectSoapServiceImpl.class);
    private static final int CONO = ConfigReader.getPropertyAsInt("sxe.cono");
    private static final String OPER = ConfigReader.getProperty("sxe.oper");

    public List<Variable> execute(WorkflowInfo workflowInfo, String command, List<Variable> inputVariables)
    {
        List<Variable> outputVariables = new ArrayList<Variable>();

        if(command != null && !command.isEmpty()) {
            switch(command) {
                case "validateProduct":
                    this.validateProduct(inputVariables, outputVariables);
                    break;
                case "createProduct":
                    this.createProduct(inputVariables, outputVariables);
                    break;
                case "getItemNetAvail":

                    break;
            }
        }

        return outputVariables;
    }

    /**
     * Validates product status
     */
    private void validateProduct(List<Variable> inputVariables, List<Variable> outputVariables)
    {
        String whse = VariableHelper.getStringValue("Warehouse", inputVariables);
        String prod = VariableHelper.getStringValue("Product", inputVariables);

        BooleanValue v1 = new BooleanValue();
        v1.setName("Status");
        v1.setValue(false);
        outputVariables.add(v1);

        StringValue v2 = new StringValue();
        v2.setName("ResultMessage");
        v2.setValue("Product is valid");
        outputVariables.add(v2);
    }

    /**
     * Copies product data from source whse to dest whse
     */
    private void createProduct(List<Variable> inputVariables, List<Variable> outputVariables)
    {
        String destWhse = VariableHelper.getStringValue("Warehouse", inputVariables);
        String prod = VariableHelper.getStringValue("Product", inputVariables);

       
    }

    /**
     * Helper methods to get and set variable values.
     */
    private static class VariableHelper
    {
        private static boolean getBooleanValue(String variableName, List<Variable> inputVariables) {
            for (Variable inputVariable : inputVariables) {
                if (inputVariable.getName().equalsIgnoreCase(variableName)) {
                    BooleanValue value = (BooleanValue) inputVariable;
                    return value.isValue();
                }
            }
            throw new RuntimeException("Variable '" + variableName + "' is missing.");
        }

        private static void setBooleanValue(String variableName, boolean value, List<Variable> outputVariables) {
            BooleanValue variable = new BooleanValue();
            variable.setName(variableName);
            variable.setValue(value);
            outputVariables.add(variable);
        }

        private static XMLGregorianCalendar getDateTimeValue(String variableName, List<Variable> inputVariables) {
            for (Variable inputVariable : inputVariables) {
                if (inputVariable.getName().equalsIgnoreCase(variableName)) {
                    DateTimeValue value = (DateTimeValue) inputVariable;
                    return value.getValue();
                }
            }
            throw new RuntimeException("Variable '" + variableName + "' is missing.");
        }

        private static void setDateTimeValue(String variableName, XMLGregorianCalendar value, List<Variable> outputVariables) {
            DateTimeValue variable = new DateTimeValue();
            variable.setName(variableName);
            variable.setValue(value);
            outputVariables.add(variable);
        }

        private static XMLGregorianCalendar getDateValue(String variableName, List<Variable> inputVariables) {
            for (Variable inputVariable : inputVariables) {
                if (inputVariable.getName().equalsIgnoreCase(variableName)) {
                    DateValue value = (DateValue) inputVariable;
                    return value.getValue();
                }
            }
            throw new RuntimeException("Variable '" + variableName + "' is missing.");
        }

        private static void setDateValue(String variableName, XMLGregorianCalendar value, List<Variable> outputVariables) {
            DateValue variable = new DateValue();
            variable.setName(variableName);
            variable.setValue(value);
            outputVariables.add(variable);
        }

        private static double getDecimalValue(String variableName, List<Variable> inputVariables) {
            for (Variable inputVariable : inputVariables) {
                if (inputVariable.getName().equalsIgnoreCase(variableName)) {
                    DecimalValue value = (DecimalValue) inputVariable;
                    return value.getValue();
                }
            }
            throw new RuntimeException("Variable '" + variableName + "' is missing.");
        }

        private static void setDecimalValue(String variableName, double value, List<Variable> outputVariables) {
            DecimalValue variable = new DecimalValue();
            variable.setName(variableName);
            variable.setValue(value);
            outputVariables.add(variable);
        }

        private static long getIntegerValue(String variableName, List<Variable> inputVariables) {
            for (Variable inputVariable : inputVariables) {
                if (inputVariable.getName().equalsIgnoreCase(variableName)) {
                    IntegerValue value = (IntegerValue) inputVariable;
                    return value.getValue();
                }
            }
            throw new RuntimeException("Variable '" + variableName + "' is missing.");
        }

        private static void setIntegerValue(String variableName, long value, List<Variable> outputVariables) {
            IntegerValue variable = new IntegerValue();
            variable.setName(variableName);
            variable.setValue(value);
            outputVariables.add(variable);
        }

        private static String getStringValue(String variableName, List<Variable> inputVariables) {
            for (Variable inputVariable : inputVariables) {
                if (inputVariable.getName().equalsIgnoreCase(variableName)) {
                    StringValue value = (StringValue) inputVariable;
                    return value.getValue();
                }
            }
            throw new RuntimeException("Variable '" + variableName + "' is missing.");
        }

        private static void setStringValue(String variableName, String value, List<Variable> outputVariables) {
            StringValue variable = new StringValue();
            variable.setName(variableName);
            variable.setValue(value);
            outputVariables.add(variable);
        }
    }
}
