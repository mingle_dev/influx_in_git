package com.influx.ws;

import com.influx.ConfigReader;
import com.influx.bean.Account;
import com.influx.bean.Product;
import com.influx.bean.Products;
import com.influx.dataobject.sx.AccountDao;
import com.influx.dataobject.sx.ProductDao;

import javax.jws.WebService;
import java.math.BigDecimal;
import java.util.ArrayList;

@WebService(serviceName = "CatalogService",
            portName = "CatalogServiceSoapBinding",
            endpointInterface = "com.influx.ws.CatalogSoapService")
public class CatalogSoapServiceImpl implements CatalogSoapService
{
    private static final String OPER = ConfigReader.getProperty("sxe.oper");

    @Override
    public Product getItemPrice(int cono, String custno, String shipto, String whse, String prod)
    {
        String[] arr = {prod};

        Products prodArr = this.getItemPriceMultiple(cono, custno, shipto, whse, arr);

        return (prodArr.getProductList().size() ==1) ? prodArr.getProductList().get(0) : null;
    }

    @Override
    public Products getItemPriceMultiple(int cono, String custno, String shipto, String whse, String[] prod)
    {
        //fetch default assigned warehouse
        if(whse == null || whse.isEmpty()) {
            AccountDao accountDao = new AccountDao();
            Account account = accountDao.getAccountInfo(cono, OPER, custno);
            whse = account.getSalesTerritory();
        }
        Products p = new Products();
        shipto = (shipto == null) ? "" : shipto;

        ProductDao productDao = new ProductDao();

        ArrayList<Product> prodList;
		try {
			prodList = (ArrayList<Product>) productDao.getSAPProducts(cono, OPER, custno, shipto, whse, prod);
			p.setProductList(prodList);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        return p;

    }
}