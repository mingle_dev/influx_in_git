package com.influx.ws;

/**
 * Created by chris on 11/19/14.
 /*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import com.influx.bean.Account;

import java.math.BigDecimal;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author chris
 */
@WebService
public interface AccountSoapService
{
    @WebMethod(operationName = "getAccountInfo")
    public @WebResult(name = "AccountInfo") Account getAccountInfo(@WebParam(name="cono") @XmlElement(required=true, nillable=false) int cono,
                                                                   @WebParam(name="oper") @XmlElement(required=true, nillable=false) String oper,
                                                                   @WebParam(name="custno") @XmlElement(required=true, nillable=false) BigDecimal custno,
                                                                   @WebParam(name="shipto") @XmlElement(required=false, nillable=true) String shipto);
}
