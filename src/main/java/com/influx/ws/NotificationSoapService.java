/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlElement;

/**
 *
 * @author chris
 */
@WebService
public interface NotificationSoapService
{
    @WebMethod(operationName = "subscribeSNSTopic")
    public boolean subscribeSNSTopic(@WebParam(name="id") String id,
                                     @WebParam(name="protocol") String protocol,
                                     @WebParam(name="endpoint") String endpoint);
    
    @WebMethod(operationName = "publishSNSTopic")
    public boolean publishSNSTopic(@WebParam(name="id") String id,
                                   @WebParam(name="message") String message);
    
    @WebMethod(operationName = "sendSMSMessage")
    public boolean sendSMSMessage(@WebParam(name="mobileNumber") @XmlElement(required=true) String mobileNumber,
                                  @WebParam(name="content") @XmlElement(required=true) String content);

    @WebMethod(operationName = "fetchEmail")
    public String fetchEmail();
}