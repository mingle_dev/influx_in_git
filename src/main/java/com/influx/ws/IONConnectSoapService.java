/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

/**
 *
 * @author chris
 */
@WebService
public interface IONConnectSoapService
{
    @WebMethod(operationName = "notifyShipment")
    public String notifyShipment(@WebParam(name="cono") String conoStr, @WebParam(name="orderno") String orderno, @WebParam(name="shipvia") String shipvia);    
}