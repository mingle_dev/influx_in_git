/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.ws;

import com.influx.bean.SyncCart;
import com.influx.service.CartService;

import javax.jws.WebService;

import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author chris
 */
@WebService(endpointInterface = "com.influx.ws.OrderSoapService")
public class OrderSoapServiceImpl implements OrderSoapService
{
    @Autowired
    CartService cartService;
    
    @Override
    public String createExternalCart(SyncCart items)
    {
        return cartService.createCart(items);
    }
    
    @Override
    public String importExternalCart(String username, String tokenId) throws Exception
    {
        return cartService.importCart(username, tokenId);
    }
}