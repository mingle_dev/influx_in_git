/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.ws;

import java.net.URI;
import java.util.List;
import javax.jws.WebService;

import microsoft.exchange.webservices.data.ExchangeCredentials;
import microsoft.exchange.webservices.data.ExchangeService;
import microsoft.exchange.webservices.data.ExchangeVersion;
import microsoft.exchange.webservices.data.FindItemsResults;
import microsoft.exchange.webservices.data.Item;
import microsoft.exchange.webservices.data.ItemView;
import microsoft.exchange.webservices.data.WebCredentials;
import microsoft.exchange.webservices.data.WellKnownFolderName;
import org.apache.log4j.Logger;

/**
 *
 * @author chris
 */
@WebService(endpointInterface = "com.influx.ws.NotificationSoapService")
public class NotificationSoapServiceImpl implements NotificationSoapService
{
    private static Logger log = Logger.getLogger(NotificationSoapService.class);
    
    @Override
    public boolean subscribeSNSTopic(String id, String protocol, String endpoint)
    {
//        try {
//            AmazonSNS sns = new AmazonSNSClient();
//            //SubscribeResult sr = sns.subscribe("arn:aws:sns:us-east-1:019231595700:mingle", "SMS", "14043680307");
//            SubscribeResult sr = sns.subscribe(id, protocol, endpoint);
//            
//            if(sr.getSubscriptionArn() != null && !sr.getSubscriptionArn().isEmpty())
//                return true;
//            
//        }catch(Exception ex) {
//            LOG.error(ex.getMessage());
//        }   
        
        return false;
    }
    
//    @WebMethod(operationName = "publishSNSTopic")
//    public boolean publishSNSTopic(@WebParam(name="id") String id,
//                                   @WebParam(name="message") String message)
//    {
//        try {
//            AmazonSNS sns = new AmazonSNSClient();
//            PublishResult pr = sns.publish(id, message);
//            
//            if(pr.getMessageId() != null && !pr.getMessageId().isEmpty())
//                return true;
//            
//        }catch(Exception ex) {
//            LOG.error(ex.getMessage());
//        }
//        
//        return false;
//    }
//    
//    /**
//     * https://www.twilio.com/
//     * username: chris.weaver@mingledorffs.com
//     * password: wstinol_669
//     * 
//     * @param mobileNumber
//     * @param content
//     * @return 
//     */
//    @WebMethod(operationName = "sendSMSMessage")
//    public boolean sendSMSMessage(@WebParam(name="mobileNumber") @XmlElement(required=true) String mobileNumber,
//                                  @WebParam(name="content") @XmlElement(required=true) String content)
//    {
//        try {
//            TwilioRestClient client = new TwilioRestClient(Constants.TWILIO_ACCOUNT_SID, Constants.TWILIO_AUTH_TOKEN); 
//
//             // Build the parameters 
//             List<NameValuePair> params = new ArrayList<NameValuePair>(); 
//             params.add(new BasicNameValuePair("To", "4043861160")); 
//             params.add(new BasicNameValuePair("From", "+15084220338")); 
//             params.add(new BasicNameValuePair("Body", "AWS won't work. This might."));   
//
//             MessageFactory messageFactory = client.getAccount().getMessageFactory(); 
//
//             Message message = messageFactory.create(params); 
//             System.out.println(message.getSid()); 
//             
//        }catch(Exception ex) {
//            System.out.println(ex);
//        }
//        
//        return true;
//    }
//    
//    /**
//     * handle: mingledorffs_it
//     * username: infotech@mingledorffs.com
//     * password: wstinol_669
//     * 
//     * @param message
//     * @return 
//     */
//    @WebMethod(operationName = "updateTwitterStatus")
//    public boolean updateTwitterStatus(@WebParam(name="message") @XmlElement(required=true) String message)
//    {
//        try {
//            Twitter twitter = TwitterFactory.getSingleton();
//            Status status = twitter.updateStatus(message);
//            LOG.info("Successfully updated the status to [" + status.getText() + "].");    
//            
//            return true;
//            
//        }catch(Exception ex) {
//            LOG.error(ex.toString());
//        }
//        
//        return false;
//    }

    @Override
    public boolean publishSNSTopic(String id, String message) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean sendSMSMessage(String mobileNumber, String content) {
        throw new UnsupportedOperationException("Not supported yet.");

        /*
        try {
            TwilioRestClient client = new TwilioRestClient(Constants.TWILIO_ACCOUNT_SID, Constants.TWILIO_AUTH_TOKEN);

             // Build the parameters
             List<NameValuePair> params = new ArrayList<NameValuePair>();
             params.add(new BasicNameValuePair("To", mobileNumber));
             params.add(new BasicNameValuePair("From", "+15084220338"));
             params.add(new BasicNameValuePair("Body", content));

             MessageFactory messageFactory = client.getAccount().getMessageFactory();

             Message message = messageFactory.create(params);
             System.out.println(message.getSid());

        }catch(Exception ex) {
            System.out.println(ex);
        }

        return true;
         */
    }
    
    @Override
    public String fetchEmail()
    {
        try {
            ExchangeService service = new ExchangeService(ExchangeVersion.Exchange2007_SP1);
            ExchangeCredentials credentials = new WebCredentials("chris.weaver@mingledorffs.com","wstinol_669");
            service.setCredentials(credentials);
            service.setUrl(new URI("https://webmail.mingledorffs.com/ews/exchange.asmx"));

            ItemView view = new ItemView (10);
            FindItemsResults findResults = service.findItems(WellKnownFolderName.Inbox, view);

            List items = findResults.getItems();
            for(int i=0; i<items.size(); i++) {
                Item item = (Item) items.get(i);
                //System.out.println(item);
            }
            //for(Item item : findResults.getItems()){
            //    item.load(new PropertySet(BasePropertySet.FirstClassProperties, ItemSchema.MimeContent));
            //    System.out.println("id==========" + item.getId());
            //    System.out.println("sub==========" + item.getSubject());
            //    System.out.println("sub==========" + item.getMimeContent());
            //}
        }catch(Exception ex) {
            log.error(ex);
        }
        
        return "done";
    }
}