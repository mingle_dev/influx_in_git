package com.influx.insite;

import java.net.URI;
import java.util.List;

import javax.ws.rs.Encoded;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.influx.ConfigReader;
import com.influx.bean.InsiteOrder;
import com.influx.bean.InsiteCartLine;


public class InsiteRestService
{
	@Autowired
	InsiteCartUtils insiteUtils;
	
	public static final String insiteHostUrl = ConfigReader.getProperty("insite.hostUrl");
	
	@GET
    @Path("/import")
   // @Produces("application/json")
	@Encoded
    public Response importCart(@QueryParam("data") String data, @QueryParam("uid") String uid, @QueryParam("email") String email) throws Exception {
		
		String result = insiteUtils.importCart(data, uid, email);
		if(result.equalsIgnoreCase("200")) {
			return Response.seeOther(new URI(insiteHostUrl + "/Cart")).build();
		} else if(result.equalsIgnoreCase("401") || result.equalsIgnoreCase("300")) {
			return Response.seeOther(new URI(insiteHostUrl + "/MyAccount/SignIn")).build();
		} else {
			return Response.ok(result).build();
		}
    }
	
	@POST
    @Path("/submitOrder")
	@Encoded
    public InsiteOrder submitOrder(InsiteOrder insiteOrder) throws Exception {
		if(insiteOrder != null && insiteOrder.getAccessToken() != null && insiteOrder.getEmail() != null && insiteOrder.getProducts()!=null && insiteOrder.getProducts().size() > 0) {
			return insiteUtils.submitOrderToInsite(insiteOrder); 
	    } else {
	    	insiteOrder.setGateWayStatus("Gateway basic data Validation Failed.");
	    	return insiteOrder;
	    }
	}
	
	@POST
    @Path("/OrderCheck")
	@Encoded
    public InsiteOrder OrderCheck(InsiteOrder insiteOrder) throws Exception {
		if(insiteOrder != null && insiteOrder.getAccessToken() != null) {
			return insiteUtils.getOrderStatus(insiteOrder); 
	    } else {
	    	insiteOrder.setGateWayStatus("Gateway basic data Validation Failed.");
	    	return insiteOrder;
	    }
	}
	
	@GET
    @Path("/getItems")
    @Produces("application/json")
	@Encoded
    public List<InsiteCartLine> getItems(@QueryParam("data") String data, @QueryParam("uid") String uid) throws Exception {
		return insiteUtils.getCartItems(data, uid);
    }
	
	@GET
    @Path("/getToken")
	@Encoded
    public String getToken(@QueryParam("data") String data, @QueryParam("uid") String uid) throws Exception {
		return insiteUtils.getTokenAlone(data, uid);
    }
	
	@GET
    @Path("/getData")
	@Encoded
    public String getData(@QueryParam("token") String data, @QueryParam("uid") String uid) throws Exception {
		return insiteUtils.getData(data, uid);
    }
	
}
