package com.influx.insite;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.influx.ConfigReader;
import com.influx.bean.InsiteCart;
import com.influx.bean.InsiteCartLine;
import com.influx.bean.InsiteOrder;
import com.influx.bean.SyncCart;
import com.influx.bean.SyncCartItem;
import com.influx.bean.User;
import com.influx.insite.URLEncodedUtils.NameValuePair;
import com.influx.service.CartService;


@Service
@Transactional
public class InsiteCartUtils
{
	@Autowired
    CartService cartService;
	
	@Autowired
    private MongoTemplate mongoTemplate;
	
	public static final String influxInsiteConnectorUrl = ConfigReader.getProperty("influxInsiteConnector.url");
	public static final String insiteGateWayUrl = ConfigReader.getProperty("insiteGateWay.url");
    private static final String insiteErrorMsg = ConfigReader.getProperty("insite.errorMsg");
    private static final String insiteCartNotFound = ConfigReader.getProperty("insite.cartNotFound");
    
    private static Logger log = Logger.getLogger(InsiteCartUtils.class);
    
    public String result = "";
    public String cartToken = "";
	
    
    public InsiteOrder submitOrderCheck(InsiteOrder insiteOrder) throws Exception
    {
    	try {
    		return submitOrderToInsite(insiteOrder);
		} catch (Exception e) {
			log.error("Gateway Data Verification failed for : " + insiteOrder.getPoNumber());
			e.printStackTrace();
		}
    	log.error("Order Submission success for : " + insiteOrder.getPoNumber());
    	return insiteOrder;
    }
   
    
    public InsiteOrder submitOrderToInsite(InsiteOrder result) {

		try {
			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
			mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM));
			restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

			HttpEntity<InsiteOrder> request = new HttpEntity<InsiteOrder>(result, headers);
			String requestUrl = insiteGateWayUrl + "/api/Order/SubmitLite";
			ResponseEntity<InsiteOrder> responseEntity = restTemplate.postForEntity(requestUrl, request, InsiteOrder.class);
			
			log.info("insite order submit result :" + responseEntity);
			result = responseEntity.getBody();
			
			if(responseEntity.getStatusCode().equals(HttpStatus.OK)) {
				result.setGateWayStatus("Gateway order submission successful");
			} else {
				result.setGateWayStatus("Gateway order submission failed");
			}
		} catch (HttpStatusCodeException e) {
	        if(e.getStatusCode().value() == 401) {
	        	result.setGateWayStatus("Gateway submission not Authorized");
	        }
		} catch (RestClientException e) {
			e.printStackTrace();
			result.setGateWayStatus("Gateway order submission failed");
		}

		return result;
	}
    
    public InsiteOrder getOrderStatus(InsiteOrder result) {

		try {
			RestTemplate restTemplate = new RestTemplate();

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
			mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM));
			restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

			HttpEntity<InsiteOrder> request = new HttpEntity<InsiteOrder>(result, headers);
			String requestUrl = insiteGateWayUrl + "/api/OrderStatus/Check";
			ResponseEntity<InsiteOrder> responseEntity = restTemplate.postForEntity(requestUrl, request, InsiteOrder.class);
			
			log.info("insite order submit result :" + responseEntity);
			result = responseEntity.getBody();
			
			if(responseEntity.getStatusCode().equals(HttpStatus.OK)) {
				result.setGateWayStatus("Gateway order Status check successful");
			} else {
				result.setGateWayStatus("Gateway order Status check failed");
			}
		} catch (HttpStatusCodeException e) {
	        if(e.getStatusCode().value() == 401) {
	        	result.setGateWayStatus("Gateway access not Authorized");
	        }
		} catch (RestClientException e) {
			e.printStackTrace();
			result.setGateWayStatus("Gateway ordrer status failed");
		}

		return result;
	}

    public String importCart(String data, String uid, String email) throws Exception
    {
    	String status = "OK";
    	try {
    		if(verifyData(data, uid)) {
				List<InsiteCartLine> cartItems = pullItems(); 
				if (cartItems != null && cartItems.size() > 0) {
					status = insitePostCart(cartItems, email);
				} else {
					status = insiteCartNotFound;
				}
    		} else {
    			status = insiteCartNotFound;
    		}
		} catch (Exception e) {
			status = insiteErrorMsg;
			log.error("Import cart failed for :" +data +" : "+status);
			e.printStackTrace();
		}
    	log.info("Import cart success for :" +data +" : "+status);
    	return status;
    }
   
    
	public String insitePostCart(List<InsiteCartLine> list, String email) {

		String statusMsg = "OK";
		try {
			RestTemplate restTemplate = new RestTemplate();
			ResponseEntity<String> result = null;

			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.setContentType(MediaType.APPLICATION_JSON);
			
			MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
			mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM));
			restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

			InsiteCart ir = new InsiteCart();
			ir.setEmail(email);
			ir.setProducts(list);
			HttpEntity<InsiteCart> request = new HttpEntity<InsiteCart>(ir, headers);
		   
			// HttpEntity<List<InsiteCartRequest>> request = new HttpEntity<List<InsiteCartRequest>>(list, headers);
			//ResponseEntity<String> result = restTemplate.exchange(insiteHostUrl, HttpMethod.POST, request, String.class);
			//result = restTemplate.postForObject(insiteHostUrl, request, String.class);
			result = restTemplate.postForEntity(influxInsiteConnectorUrl, request, String.class);
			
			log.info("insite post cart result :" + result);
			
			if(result.getStatusCode().equals(HttpStatus.OK)) {
				if(result.getBody().equals("No access token.")) {
					statusMsg = "300";
				} else {
					statusMsg = "200";
				}
			} else {
				statusMsg = insiteErrorMsg;
			}
		} catch (HttpStatusCodeException e) {
	        if(e.getStatusCode().value() == 401) {
	        	log.info("insite post cart failed with 401...");
	        	statusMsg = "401";
	        }
		} catch (RestClientException e) {
			e.printStackTrace();
			log.info("insite post cart failed..." + e.getMessage());
			statusMsg = insiteErrorMsg;
		}

		return statusMsg;
	}
    
	public List<InsiteCartLine> pullItems() throws Exception
    {
    	List<InsiteCartLine> cartItems = new ArrayList<InsiteCartLine>();
    	
    	try {
			SyncCart sc = cartService.getCart(cartToken);
			
			if (sc != null && sc.getItems().size() > 0) {
				for (SyncCartItem scl : sc.getItems())
					cartItems.add(new InsiteCartLine(scl.getItemName(), scl.getQuantity()));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	log.info("insite pullItems successful...");
    	return cartItems;
    }
	
	 // Copied this stuff from WZ to matchup existing verification process...
    public boolean verifyData(String data, String uid)
    {
    	try {
			User infxUser = getInfluxById(uid);
			if(infxUser != null) {
				cartToken = "";
				String decryptData = Encryption.DES3Decrypt(data, infxUser.getSecurityKey());
	
				//store data into key/value pair array <-- why not HashMap???
				if(decryptData != null) {
					List<NameValuePair> nvpList = URLEncodedUtils.parse(decryptData, "UTF-8");
		
					//get influx username/password from data-feed and
					//compare to username/password stored in influx db; if match continue
					String sfUser = nvpList.get(nvpList.indexOf(new NameValuePair("username", null))).getValue();
					String sfPass = nvpList.get(nvpList.indexOf(new NameValuePair("password", null))).getValue();
		
					if (sfUser.equals(infxUser.getUsername()) && sfPass.equals(infxUser.getPassword())) {
						cartToken = nvpList.get(nvpList.indexOf(new NameValuePair("token", null))).getValue();
		                if (cartToken != null) {
		                	log.info("insite verifyData successful...");
		                	return true;
						} else {
							log.info("insite verifyData failed...");
							return false;
						}
					} else {
						log.info("insite verifyData auth failed...");
					    return false;
					} 
				} else {
					log.info("insite verifyData decrypt failed...");
					return false;
				}
			} else {
				log.info("insite verifyData no user found...");
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			log.info("insite verifyData Error...");
			 return false;
		}
    }
    
    public com.influx.bean.User getInfluxById(String userId)
    {
    	return mongoTemplate.findById(userId, com.influx.bean.User.class, "user_authorization");
    }
   
    
    
	// from here until the end is for testing purpose only so we can figure out encrypted and decrypted token if required.
    // see all items under that cart for immediate testing when required. 
    public List<InsiteCartLine> getCartItems(String data, String uid) throws Exception
    {
    	List<InsiteCartLine> list = new ArrayList<InsiteCartLine>();
    	
    	try {
			if (verifyData(data, uid)) {
				return pullItems(); 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	
    	return list;
    }
    
    public String getTokenAlone(String data, String uid) throws Exception
    {
    	String tokenAlone="";
    	try {
    		User infxUser = getInfluxById(uid);

			String decryptData = Encryption.DES3Decrypt(data, infxUser.getSecurityKey());
			String[] array = decryptData.split("\\&", -1);
			for(String str : array) {
				if(str.contains("token=")){
					String[] array1 = str.split("\\=", -1);
					tokenAlone = array1[1];
					break;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return tokenAlone;
    }
    
    // for testing...
    public String getData(String data, String uid) throws Exception
    {
    	String fullData="";
    	User infxUser = getInfluxById(uid);
    	String input = "username="+infxUser.getUsername()+"&password="+ infxUser.getPassword() +"&token="+data;
    	try {
    		fullData = Encryption.DES3Encrypt(input, infxUser.getSecurityKey()); 
		} catch (Exception e) {
			e.printStackTrace();
		}
    	return fullData;
    }
    
   
       
}