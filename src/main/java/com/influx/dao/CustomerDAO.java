package com.influx.dao;

import com.influx.bean.Customer;

public interface CustomerDAO {
	
	public Customer findCustomer(String custno, String cono);
}
