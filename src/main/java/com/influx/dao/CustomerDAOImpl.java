package com.influx.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.datasource.DriverManagerDataSource;

import com.influx.ConfigReader;
import com.influx.bean.Customer;

public class CustomerDAOImpl implements CustomerDAO {

	public static final String dbDriver = ConfigReader.getProperty("SQL.DRIVER");
	public static final String dbUrl = ConfigReader.getProperty("SQL.URL");
	public static final String dbUser = ConfigReader.getProperty("SQL.USER");
	public static final String dbPass = ConfigReader.getProperty("SQL.PASS");

	
	
	@Override
	public Customer findCustomer(String custno, String cono) {
		String sql = "SELECT * FROM SAPCustomers WHERE CustomerNumber = ? and cono = ?";
		
		Connection conn = null;
		
		try {

			DriverManagerDataSource dataSource = new DriverManagerDataSource();
	        dataSource.setDriverClassName(dbDriver);
	        dataSource.setUrl(dbUrl);
	        dataSource.setUsername(dbUser);
	        dataSource.setPassword(dbPass);
	        
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, custno);
			ps.setString(2, cono);
			Customer customer = null;
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				customer = new Customer(
					rs.getString("CustomerName"),
					rs.getString("CustomerType"), 
					rs.getString("Phone"), 
					rs.getString("Address1"),
					rs.getString("Address2"), 
					rs.getString("City"),
					rs.getString("State"), 
					rs.getString("Zip"), 
					rs.getString("TermsType"),
					rs.getString("Status"),
					rs.getString("Plant")
				);
			}
			rs.close();
			ps.close();
			return customer;
		} catch (SQLException e) {
			throw new RuntimeException(e);
		} finally {
			if (conn != null) {
				try {
				conn.close();
				} catch (SQLException e) {}
			}
		}
	
	}
	

}
