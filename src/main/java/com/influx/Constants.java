package com.influx;

import java.util.HashMap;

public class Constants
{
    private static final HashMap<String,String> printerMap;

    static
    {
        printerMap = new HashMap<String, String>();
        printerMap.put("21", "norpic");
        printerMap.put("22", "MarCtr");
        printerMap.put("24", "forgbar"); //fixed; 2013.10.25
        printerMap.put("28", "law196");
        printerMap.put("32", "gaingbar");
        printerMap.put("34", "airgbar");
        printerMap.put("36", "athngbar");
        printerMap.put("37", "douggb");
        printerMap.put("38", "congb");
        printerMap.put("41", "maconlaser"); //fixed; 2013.10.25
        printerMap.put("44", "wrlaser"); //added; 2013.10.25
        printerMap.put("46", "colgbar");
        printerMap.put("48", "albygbar");
        printerMap.put("49", "49cl");
        printerMap.put("51", "51cl");
        printerMap.put("52", "52cl");
        printerMap.put("53", "53cl");
        printerMap.put("54", "54cl");
        printerMap.put("55", "55cl");
        printerMap.put("56", "56cl");
        printerMap.put("57", "57cl");
        printerMap.put("58", "58cl");
        printerMap.put("59", "59cl");
        printerMap.put("61", "SavCtr");
        printerMap.put("64", "auggbar");
        printerMap.put("66", "hlthgbar");
        printerMap.put("68", "stagbar");
        printerMap.put("72", "valgbar");
        printerMap.put("73", "brurpt");
        printerMap.put("81", "birrpt");
        printerMap.put("82", "hunrpt");
        printerMap.put("83", "musrpt");
        printerMap.put("86", "86cl");
    }

    public static String getWhsePrinter(String id)
    {
        String res = printerMap.get(id.trim());

        return (res == null) ? "" : printerMap.get(id);
    }
}
