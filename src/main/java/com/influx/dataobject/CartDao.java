package com.influx.dataobject;

import org.skife.jdbi.v2.sqlobject.Bind;
import org.skife.jdbi.v2.sqlobject.SqlQuery;

/**
 * Created by chris on 11/7/14.
 */
public interface CartDao
{
    public void getCart(String userId);

    @SqlQuery("select last_name from contacts where id = :id")
    String findById(@Bind("id") String id);
}