/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.dataobject.sx;

import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.List;

import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.influx.ConfigReader;
import com.influx.bean.Account;
import com.influx.bean.BalanceResponse;
import com.influx.bean.Customer;
import com.influx.bean.Order;
import com.influx.bean.OrderLine;
import com.influx.bean.OrderType;
import com.influx.bean.SAPPeriodBalanceResponse;
import com.influx.dao.CustomerDAO;
import com.influx.dao.CustomerDAOImpl;
import com.infor.sxapi2.paramclass.ARGetCustomerBalanceV2.input.ARGetCustomerBalanceV2Request;
import com.infor.sxapi2.paramclass.ARGetCustomerBalanceV2.output.ARGetCustomerBalanceV2Response;
import com.infor.sxapi2.paramclass.ARGetCustomerDataCredit.input.ARGetCustomerDataCreditRequest;
import com.infor.sxapi2.paramclass.ARGetCustomerDataCredit.output.ARGetCustomerDataCreditResponse;
import com.infor.sxapi2.paramclass.ARGetCustomerDataGeneral.input.ARGetCustomerDataGeneralRequest;
import com.infor.sxapi2.paramclass.ARGetCustomerDataGeneral.output.ARGetCustomerDataGeneralResponse;
import com.infor.sxapi2.paramclass.ARGetCustomerDataOrdering.input.ARGetCustomerDataOrderingRequest;
import com.infor.sxapi2.paramclass.ARGetCustomerDataOrdering.output.ARGetCustomerDataOrderingResponse;
import com.infor.sxapi2.paramclass.OEGetSingleOrderV3.input.OEGetSingleOrderV3Request;
import com.infor.sxapi2.paramclass.OEGetSingleOrderV3.output.AvailableField;
import com.infor.sxapi2.paramclass.OEGetSingleOrderV3.output.LineItem;
import com.infor.sxapi2.paramclass.OEGetSingleOrderV3.output.OEGetSingleOrderV3Response;
import com.infor.sxapi2.paramclass.WTGetSingleTransferOrder.input.WTGetSingleTransferOrderRequest;
import com.infor.sxapi2.paramclass.WTGetSingleTransferOrder.output.WTGetSingleTransferOrderResponse;
import com.infor.sxapi2.services.callobject.ARGetCustomerBalanceV2;
import com.infor.sxapi2.services.callobject.ARGetCustomerDataCredit;
import com.infor.sxapi2.services.callobject.ARGetCustomerDataGeneral;
import com.infor.sxapi2.services.callobject.ARGetCustomerDataOrdering;
import com.infor.sxapi2.services.callobject.OEGetSingleOrderV3;
import com.infor.sxapi2.services.callobject.WTGetSingleTransferOrder;

/**
 *
 * @author chris.weaver
 */
public class AccountDao
{
    private static Logger log = Logger.getLogger(AccountDao.class);
    
    public static final String sapHost = ConfigReader.getProperty("sap.host");
    public static final String sapUser = ConfigReader.getProperty("sap.user");
    public static final String sapPass = ConfigReader.getProperty("sap.pass");


    /**
     *
     */
    
    public Account getAccountInfo(int cono, String oper, String custno)
    {
        Account acct = new Account();
        SxAPIWorker sxapi = null;

      

        return acct;
    }
    
    public static CloseableHttpClient getCloseableHttpClient()
    {
    	CloseableHttpClient httpClient = null;
    	try {
    		httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
    		        .setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy()
    		        {
    		            public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException
    		            {
    		                return true;
    		            }

						@Override
						public boolean isTrusted(java.security.cert.X509Certificate[] chain, String authType)
								throws java.security.cert.CertificateException {
							// TODO Auto-generated method stub
							return true;
						}
    		        }).build()).build();

    	} catch (KeyManagementException e) {
    		 log.info("KeyManagementException in creating http client instance", e);
    	} catch (NoSuchAlgorithmException e) {
    		 log.info("NoSuchAlgorithmException in creating http client instance", e);
    	} catch (KeyStoreException e) {
    		 log.info("KeyStoreException in creating http client instance", e);
    	}
    	return httpClient;
    }
    
    public Account getSAPAccountInfo(int cono, String oper, BigDecimal custno)
    {
        Account acct = new Account();
 
        try {
             
        	CustomerDAO customerDAO = new CustomerDAOImpl();
        	
        	Customer cust = customerDAO.findCustomer(custno.toString(), "1000");
        	
        	acct.setCustno(custno);
            acct.setName(cust.getCustomerName());
            acct.setAddress1(cust.getAddress1());
            acct.setAddress2(cust.getAddress2());
            acct.setCity(cust.getCity());
            acct.setState(cust.getState());
            acct.setZip(cust.getZip());
            acct.setStatusType(cust.getTermsType());
            acct.setPhoneNumber(cust.getPhone());
            acct.setTermsDescrip(cust.getTermsType());
            acct.setSalesTerritory(cust.getPlant());
        	
        	
            CloseableHttpClient client = getCloseableHttpClient();
            
            ObjectMapper mapper = new ObjectMapper();
    		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            
            ObjectNode mainNode = mapper.createObjectNode();
			ObjectNode rootNode = mapper.createObjectNode();


			rootNode.put("CustomerNumber", custno);
			rootNode.put("CompanyCode", cono);

			mainNode.set("Request", rootNode);
			String jsonString = mapper.writeValueAsString(mainNode);

			HttpPost post = new HttpPost(sapHost);
			String authInput = sapUser + ":" + sapPass ;
			String encodedString = Base64.getEncoder().encodeToString(authInput.getBytes());
			 
		    post.setHeader("Accept", "application/json");
		    post.setHeader("Content-type", "application/json");
		    post.setHeader("Authorization", "Basic "+encodedString);
			 
		    StringEntity reqEntity = new StringEntity(jsonString);
		    post.setEntity(reqEntity);
			
		    CloseableHttpResponse response = client.execute(post);
			if(response.getStatusLine().getStatusCode() == 200) {
				HttpEntity respEntity = response.getEntity();
			    if (respEntity != null) {
			    	String result = EntityUtils.toString(respEntity);
			    	JSONObject jsonObject = new JSONObject(result)
							.getJSONObject("RequestSet")
							.getJSONObject("Request")
							.getJSONObject("NV_RESPONSE");
			    	
			    	if(jsonObject.has("Response")) {
						String reply = jsonObject.toString();
						SAPPeriodBalanceResponse sapResp = mapper.readValue(reply, SAPPeriodBalanceResponse.class);
						BalanceResponse resp = sapResp.getResponse();
						
						acct.setCredLimit(new BigDecimal(resp.getCreditLimit()));
			            acct.setLastPayamt(new BigDecimal(resp.getLastPayment()));
			            acct.setOrdBal(new BigDecimal(resp.getOpenOrders()));
			            acct.setFutBal(new BigDecimal(resp.getFutureDue()));
			            acct.setPeriod1Bal(new BigDecimal(resp.getCurrentDue()));
			            acct.setPeriod1Text("Current");
			            acct.setPeriod2Bal(new BigDecimal(resp.getThirtyDays()));
			            acct.setPeriod2Text("30 Days");
			            acct.setPeriod3Bal(new BigDecimal(resp.getSixtyDays()));
			            acct.setPeriod3Text("60 Days");
			            acct.setPeriod4Bal(new BigDecimal(resp.getNinetyDays()));
			            acct.setPeriod4Text("90 Days");
			            acct.setPeriod5Bal(new BigDecimal(resp.getHunderedandTwentyPlusDays()));
			            acct.setPeriod5Text("120+ Days");
			            acct.setTotalBal(new BigDecimal(resp.getTotalDue()));
			            
			           // acct.setShipToRequiredFlag(resOrd.getShipToRequiredFlag());
			           // acct.setDefaultShipTo(resOrd.getDefaultShipTo());
			           // acct.setPoRequiredFlag(resOrd.getPoRequiredFlag());
			           // acct.setCustomerPoNo(resOrd.getCustomerPoNo());  
			           // acct.setSlsRepOut(resOrd.getSlsRepOut());
					}		    	
			    }
			}
        }catch(Exception ex) {
            log.error(ex.getMessage());
        }

        return acct;
    }

    /**
     *
     */
    public Order getOrder(int cono, String oper, BigDecimal custno, int orderno, int ordersuf) {
        Order order = this.getOrder(cono, oper, orderno, ordersuf);

        //got an order but it isn't for the requesting customer #
        if (custno.equals(order.getCustNo()) == false)
            return null;

        return order;
    }

    /**
     *
     */
    public Order getOrder(int cono, String oper, int orderno, int ordersuf) {
        SxAPIWorker sxapi = null;
        Order order = null;

        try {
            sxapi = new SxAPIWorker();

            order = new Order();
            order.setCono(cono);
            order.setOrderNo(orderno);
            order.setOrderSuf(ordersuf);

            OEGetSingleOrderV3Request req = new OEGetSingleOrderV3Request();
            req.setOrderNumber(orderno);
            req.setOrderSuffix(ordersuf);
            req.setIncludeHeaderData(true);
            req.setIncludeLineData(true);
            req.setIncludeTaxData(true);
            req.setIncludeTotalData(true);

            OEGetSingleOrderV3 obj = new OEGetSingleOrderV3();

            OEGetSingleOrderV3Response res = obj.prepareResults(sxapi.get(), "CONO=" + cono + "|OPER=" + oper, "", req);

            //all dates in SX are in format mm/dd/yy
            SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");

            //header
            for (AvailableField field : res.getListAvailableField()) {
                if (field.getFieldName().equals("stage")) {
                    if (field.getFieldValue().equals("Inv"))
                        order.setStageCode(4);
                }

                if (field.getFieldName().equals("transtype")) {
                    order.setTransType(field.getFieldValue());
                }

                try {
                    if (field.getFieldName().equals("enterdt")) {
                        order.setEnterDt(formatter.parse(field.getFieldValue()));
                    }
                } catch (Exception ig) {
                }
                try {
                    if (field.getFieldName().equals("shipdt")) {
                        order.setShipDt(formatter.parse(field.getFieldValue()));
                    }
                } catch (Exception ig) {
                }
                //try { if(field.getFieldName().equals("reqshipdt")) { order.setReqShipDt(formatter.parse(field.getFieldValue())); } }catch(Exception ig) {}

                if (field.getFieldName().equals("custpo")) {
                    order.setCustPo(field.getFieldValue());
                }
                if (field.getFieldName().equals("custno")) {
                    order.setCustNo(new BigDecimal(field.getFieldValue()));
                }
                if (field.getFieldName().equals("shipviatydesc")) {
                    order.setShipVia(field.getFieldValue());
                }
                if (field.getFieldName().equals("termstypedesc")) {
                    order.setTermsDesc(field.getFieldValue());
                }
                if (field.getFieldName().equals("whse")) {
                    order.setWhse(field.getFieldValue());
                }
                if (field.getFieldName().equals("shipinstr")) {
                    order.setInstructions(field.getFieldValue());
                }
                if (field.getFieldName().equals("refer")) {
                    order.setReference(field.getFieldValue());
                }

                if (field.getFieldName().equals("totlineamt")) {
                    if (field.getFieldValue().endsWith("-"))
                        order.setSubTotal(new BigDecimal(field.getFieldValue().substring(1, field.getFieldValue().length() - 1).trim()).negate());
                    else
                        order.setSubTotal(new BigDecimal(field.getFieldValue().trim()));
                }
                if (field.getFieldName().equals("taxamt")) {
                    if (field.getFieldValue().endsWith("-"))
                        order.setTaxAmount(new BigDecimal(field.getFieldValue().substring(1, field.getFieldValue().length() - 1).trim()).negate());
                    else
                        order.setTaxAmount(new BigDecimal(field.getFieldValue().trim()));
                }
                if (field.getFieldName().equals("totinvamt")) {
                    if (field.getFieldValue().endsWith("-"))
                        order.setInvAmount(new BigDecimal(field.getFieldValue().substring(1, field.getFieldValue().length() - 1).trim()).negate());
                    else
                        order.setInvAmount(new BigDecimal(field.getFieldValue().trim()));
                }

                if (field.getFieldName().equals("soldtonm")) {
                    order.setSoldToName(field.getFieldValue());
                }
                if (field.getFieldName().equals("soldtoaddr1")) {
                    order.setSoldToAddr1(field.getFieldValue());
                }
                if (field.getFieldName().equals("soldtoaddr2")) {
                    order.setSoldToAddr2(field.getFieldValue());
                }
                if (field.getFieldName().equals("soldtocity")) {
                    order.setSoldToCity(field.getFieldValue());
                }
                if (field.getFieldName().equals("soldtost")) {
                    order.setSoldToState(field.getFieldValue());
                }
                if (field.getFieldName().equals("soldtozipcd")) {
                    order.setSoldToZip(field.getFieldValue());
                }

                if (field.getFieldName().equals("shiptonm")) {
                    order.setShipToName(field.getFieldValue());
                }
                if (field.getFieldName().equals("shiptoaddr1")) {
                    order.setShipToAddr1(field.getFieldValue());
                }
                if (field.getFieldName().equals("shiptoaddr2")) {
                    order.setShipToAddr2(field.getFieldValue());
                }
                if (field.getFieldName().equals("shiptocity")) {
                    order.setShipToCity(field.getFieldValue());
                }
                if (field.getFieldName().equals("shiptost")) {
                    order.setShipToState(field.getFieldValue());
                }
                if (field.getFieldName().equals("shiptozip")) {
                    order.setShipToZip(field.getFieldValue());
                }
            }

            //lines -- could throw exception if res.getListLineItem() == null
            for (LineItem item : res.getListLineItem()) {
                OrderLine line = new OrderLine();
                line.setShipProd(item.getProductCode());
                line.setLineNumber(item.getLineNumber());
                line.setDescription(item.getDescription1() + " " + item.getDescription2());
                line.setQtyOrder(item.getQuantityOrdered());
                line.setQtyShip(item.getQuantityShip());
                line.setPrice(item.getPrice());
                line.setNetAmount(item.getNetAmount());

                order.addOrderLine(line);
            }

        }catch(Exception ex) {
            log.error(ex.getMessage());
        }finally{
            sxapi.release();
        }

        return order;
    }

    /**
     *
     */
    public Order getOrder(int cono, String oper, OrderType type, int orderno, int ordersuf) throws Exception
    {
        SxAPIWorker sxapi = null;

        Order order = null;

        try {
            sxapi = new SxAPIWorker();

            if (type == OrderType.WT) {
                WTGetSingleTransferOrderRequest req = new WTGetSingleTransferOrderRequest();
                req.setWarehouseTransferNumber(orderno);
                req.setWarehouseTransferSuffix(ordersuf);
                req.setIncludeHeaderData(true);
                req.setIncludeTotalData(true);
                req.setIncludeLineData(true);

                WTGetSingleTransferOrder obj = new WTGetSingleTransferOrder();

                WTGetSingleTransferOrderResponse res = obj.prepareResults(sxapi.get(), "CONO=" + cono + "|OPER=" + oper, "", req);

                if (!res.getErrorMessage().isEmpty())
                    return null;

                order = new Order();
                order.setCono(cono);
                order.setOrderNo(orderno);
                order.setOrderSuf(ordersuf);
                order.setOrderType(type);

                List<com.infor.sxapi2.paramclass.WTGetSingleTransferOrder.output.AvailableField> fields = res.getListAvailableField();
                for (com.infor.sxapi2.paramclass.WTGetSingleTransferOrder.output.AvailableField f : fields) {
                    if (f.getFieldName().equals("stage")) {
                        order.setStageCode(xlateStageCode(f.getFieldValue()));
                    }
                    if (f.getFieldName().equals("shipfmwhse")) {
                        order.setWhse(f.getFieldValue());
                    }
                    if (f.getFieldName().equals("shipviaty")) {
                        order.setShipVia(f.getFieldValue());
                    }
                    if (f.getFieldName().equals("transtype")) {
                        order.setTransType(f.getFieldValue());
                    }
                }

            } else if (type == OrderType.OE) {

                OEGetSingleOrderV3Request req = new OEGetSingleOrderV3Request();
                req.setOrderNumber(orderno);
                req.setOrderSuffix(ordersuf);
                req.setIncludeHeaderData(true);
                req.setIncludeTotalData(false);
                req.setIncludeLineData(false);

                OEGetSingleOrderV3 obj = new OEGetSingleOrderV3();

                OEGetSingleOrderV3Response res = obj.prepareResults(sxapi.get(), "CONO=" + cono + "|OPER=" + oper, "", req);

                if (!res.getErrorMessage().isEmpty())
                    return null;

                order = new Order();
                order.setCono(cono);
                order.setOrderNo(orderno);
                order.setOrderSuf(ordersuf);
                order.setOrderType(type);

                List<com.infor.sxapi2.paramclass.OEGetSingleOrderV3.output.AvailableField> fields = res.getListAvailableField();
                for (com.infor.sxapi2.paramclass.OEGetSingleOrderV3.output.AvailableField f : fields) {
                    if (f.getFieldName().equals("stage")) {
                        order.setStageCode(xlateStageCode(f.getFieldValue()));
                    }
                    if (f.getFieldName().equals("whse")) {
                        order.setWhse(f.getFieldValue());
                    }
                    if (f.getFieldName().equals("shipviaty")) {
                        order.setShipVia(f.getFieldValue());
                    }
                }
            }
        }catch(Exception ex) {
            log.error(ex.getMessage());
            throw new Exception(ex);
        }finally{
            sxapi.release();
        }

        return order;
    }

    /**
     * temporary hack
     */
    private int xlateStageCode(String stage)
    {
        if (stage.equalsIgnoreCase("Pck"))
            return 2;
        else if (stage.equalsIgnoreCase("Shp"))
            return 3;
        else if (stage.equalsIgnoreCase("Inv"))
            return 4;
        else if (stage.equalsIgnoreCase("Pd"))
            return 5;
        else if (stage.equalsIgnoreCase("Rcv"))
            return 6;
        else return -1;
    }
}