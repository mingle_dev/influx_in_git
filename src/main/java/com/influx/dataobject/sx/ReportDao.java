/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.dataobject.sx;

import com.influx.Constants;
import com.influx.bean.OrderType;
import com.infor.sxapi2.callobject.ICallObject;
import com.infor.sxapi2.paramclass.SASubmitReport.input.Option;
import com.infor.sxapi2.paramclass.SASubmitReport.input.Range;
import com.infor.sxapi2.paramclass.SASubmitReport.input.SASubmitReportRequest;
import com.infor.sxapi2.paramclass.SASubmitReport.output.SASubmitReportResponse;
import com.infor.sxapi2.paramclass.SxApiAuthenticationException;
import com.infor.sxapi2.paramclass.SxApiAuthorizationException;
import com.infor.sxapi2.paramclass.SxApiConnectionException;
import com.infor.sxapi2.paramclass.SxApiGeneralException;
import com.infor.sxapi2.services.callobject.SASubmitReport;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author chris
 */
public class ReportDao
{
    private static Logger log = Logger.getLogger(ReportDao.class);

    /**
     * 
     * @param type
     * @param orderno
     * @param whse
     * @return
     * @throws Exception 
     */
    public String printBOL(int cono, String oper, OrderType type, int orderno, String whse) throws Exception
    {
        SxAPIWorker sxapi = null;

        try {
            sxapi = new SxAPIWorker();

            SASubmitReportRequest req = new SASubmitReportRequest();

            String order = String.valueOf(orderno);

            //figure out what printer to send it to
            String whsePrinter = Constants.getWhsePrinter(whse);
            if(whsePrinter.isEmpty()) {
                req.setOutputType("e-mail");
                req.setOutputName("chris.weaver@mingledorffs.com");
            }else{
                req.setOutputType("REPORT");
                req.setOutputName(whsePrinter);
            }

            if(type == OrderType.WT) {
                req.setReportName("wtepl");

                List<Range> ranges = new ArrayList<Range>();
                ranges.add( new Range(1, "WT #", "i", true, 8, order, order) );
                ranges.add( new Range(2, "Ship From Whse", "w", true, 4, whse, whse) );
                req.setListRange(ranges);

                List<Option> options = new ArrayList<Option>();
                options.add( new Option(1, "Print Hazardous Material Sheet Info?", "YES", "q", false, 3) );
                req.setListOption(options);

            }else if(type == OrderType.OE) {
                req.setReportName("oeepl");

                List<Range> ranges = new ArrayList<Range>();
                ranges.add( new Range(1, "Customer #", "c", false, 12, "", "") );
                ranges.add( new Range(2, "Order #", "i", false, 8, order, order) );
                ranges.add( new Range(3, "Ship From Whse", "w", false, 4, whse, whse) );
                req.setListRange(ranges);

                List<Option> options = new ArrayList<Option>();
                options.add( new Option(1, "(O)rder #, (R)oute, or (E)ntry Order", "O", "a", false, 3) );
                options.add( new Option(2, "Enter a List of Order #'s?", "NO", "q", false, 3) );
                options.add( new Option(3, "Print Hazardous Material Sheet Info?", "YES", "q", false, 3) );
                req.setListOption(options);            

            }else {
                throw new Exception("Unknown OrderType");
            }

            SASubmitReport obj = new SASubmitReport();

            SASubmitReportResponse res = obj.prepareResults(sxapi.get(), "CONO=" + cono + "|OPER=" + oper, "", req);

            if( res == null || !res.getErrorMessage().isEmpty() )
                throw new Exception( res.getErrorMessage() );

            return res.getErrorMessage();

        }catch(Exception ex) {
            log.error(ex.getMessage());
            throw new Exception(ex);
        }finally{
            sxapi.release();
        }
    }
}