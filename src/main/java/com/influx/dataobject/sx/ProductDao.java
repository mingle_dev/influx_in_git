/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.dataobject.sx;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.*;

import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.ssl.TrustStrategy;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.influx.ConfigReader;
import com.influx.bean.Product;
import com.influx.bean.sapModel.PricingRecord;
import com.infor.sxapi2.paramclass.ICGetProductDataGeneralV2.input.ICGetProductDataGeneralV2Request;
import com.infor.sxapi2.paramclass.ICGetProductDataGeneralV2.output.ICGetProductDataGeneralV2Response;
import com.infor.sxapi2.paramclass.ICGetWhseProductDataCosts.input.ICGetWhseProductDataCostsRequest;
import com.infor.sxapi2.paramclass.ICGetWhseProductDataCosts.output.ICGetWhseProductDataCostsResponse;
import com.infor.sxapi2.paramclass.ICGetWhseProductDataGeneralV2.input.ICGetWhseProductDataGeneralV2Request;
import com.infor.sxapi2.paramclass.ICGetWhseProductDataGeneralV2.output.ICGetWhseProductDataGeneralV2Response;
import com.infor.sxapi2.paramclass.ICProductAvailByWhse.input.ICProductAvailByWhseRequest;
import com.infor.sxapi2.paramclass.ICProductAvailByWhse.output.ICProductAvailByWhseResponse;
import com.infor.sxapi2.paramclass.ICProductMnt.input.FieldModification;
import com.infor.sxapi2.paramclass.ICProductMnt.input.ICProductMntRequest;
import com.infor.sxapi2.paramclass.ICProductMnt.output.ICProductMntResponse;
import com.infor.sxapi2.paramclass.OEPricingMultiple.input.OEPricingMultipleRequest;
import com.infor.sxapi2.paramclass.OEPricingMultiple.output.OEPricingMultipleResponse;
import com.infor.sxapi2.services.callobject.ICGetProductDataGeneralV2;
import com.infor.sxapi2.services.callobject.ICGetWhseProductDataCosts;
import com.infor.sxapi2.services.callobject.ICGetWhseProductDataGeneralV2;
import com.infor.sxapi2.services.callobject.ICProductAvailByWhse;
import com.infor.sxapi2.services.callobject.ICProductMnt;
import com.infor.sxapi2.services.callobject.OEPricingMultiple;


/**
 *
 * @author chris.weaver
 */
public class ProductDao
{
    private static Logger log = Logger.getLogger(ProductDao.class);
    
    public static final String sapHost = ConfigReader.getProperty("sap.hostPrice");
    public static final String sapUser = ConfigReader.getProperty("sap.user");
    public static final String sapPass = ConfigReader.getProperty("sap.pass");

    /**
     *
     */
    public Product getProduct(int cono, String oper, BigDecimal custno, String shipTo, String whse, String product) {
        String[] prodArr = {product};
        List<Product> list = this.getProducts(cono, oper, custno, shipTo, whse, prodArr);

        return (list != null && list.isEmpty()) ? list.get(0) : null;
    }

    /**
     *
     */
    public List<Product> getProducts(int cono, String oper, BigDecimal custno, String shipTo, String whse, String[] prodArr)
    {
        SxAPIWorker sxapi = null;

        List<Product> products = new ArrayList<>();

        try {
            sxapi = new SxAPIWorker();

            OEPricingMultipleRequest req = new OEPricingMultipleRequest();
            req.setCustomerNumber(custno);
            req.setShipTo(shipTo);

            //add products to sxapi product class
            List<com.infor.sxapi2.paramclass.OEPricingMultiple.input.Product> inList = new ArrayList<com.infor.sxapi2.paramclass.OEPricingMultiple.input.Product>(prodArr.length);
            for (String p : prodArr) {
                com.infor.sxapi2.paramclass.OEPricingMultiple.input.Product prod = new com.infor.sxapi2.paramclass.OEPricingMultiple.input.Product();
                prod.setProductCode(p);
                prod.setQuantity(BigDecimal.ONE);
                prod.setWarehouse(whse);
                inList.add(prod);
            }
            req.setListProduct(inList);

            OEPricingMultiple obj = new OEPricingMultiple();

            OEPricingMultipleResponse res = obj.prepareResults(sxapi.get(), "CONO=" + cono + "|OPER=" + oper, "", req);

            List<com.infor.sxapi2.paramclass.OEPricingMultiple.output.Price> prices = res.getListPrice();
            for (com.infor.sxapi2.paramclass.OEPricingMultiple.output.Price p : prices) {
                //@todo; there's a chance that the price = 0 due to not setup in pricing whse
                //follow logic in getProduct() to deal with this issue

                Product fp = new Product();
                fp.setProduct(p.getProductCode());
                fp.setPrice(p.getPrice());
                fp.setPriceWhse(whse);
                fp.setAvail(p.getNetAvailable());

                products.add(fp);
            }

        }catch(Exception ex) {
            log.error(ex.getMessage());
        }finally{
            sxapi.release();
        }

        return products;
    }
    
    public static CloseableHttpClient getCloseableHttpClient()
    {
    	CloseableHttpClient httpClient = null;
    	try {
    		httpClient = HttpClients.custom().setSSLHostnameVerifier(NoopHostnameVerifier.INSTANCE)
    		        .setSSLContext(new SSLContextBuilder().loadTrustMaterial(null, new TrustStrategy()
    		        {
    		            public boolean isTrusted(X509Certificate[] arg0, String arg1) throws CertificateException
    		            {
    		                return true;
    		            }

						@Override
						public boolean isTrusted(java.security.cert.X509Certificate[] chain, String authType)
								throws java.security.cert.CertificateException {
							// TODO Auto-generated method stub
							return true;
						}
    		        }).build()).build();

    	} catch (KeyManagementException e) {
    		 log.info("KeyManagementException in creating http client instance", e);
    	} catch (NoSuchAlgorithmException e) {
    		 log.info("NoSuchAlgorithmException in creating http client instance", e);
    	} catch (KeyStoreException e) {
    		 log.info("KeyStoreException in creating http client instance", e);
    	}
    	return httpClient;
    }
    
    
    public List<Product> getSAPProducts1(int cono, String oper, BigDecimal custno, String shipTo, String whse, String[] prodArr)
    {
   
        List<Product> products = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
        try {
            
        	CloseableHttpClient client = getCloseableHttpClient();
			HttpPost post = new HttpPost(sapHost);
			String authInput = sapUser + ":" + sapPass ;
			String encodedString = Base64.getEncoder().encodeToString(authInput.getBytes());
			 
		    post.setHeader("Accept", "application/json");
		    post.setHeader("Content-type", "application/json");
		    post.setHeader("Authorization", "Basic "+encodedString);
			 
		    
			
		    if (prodArr != null && prodArr.length > 0) {
		    	for(String pr : prodArr) {
		    		
		    		Product prod = new Product();
					prod.setProduct(pr);
					prod.setAvail(BigDecimal.ZERO);
					prod.setPriceWhse(whse);
					prod.setCono(cono);

					ObjectNode mainNode = mapper.createObjectNode();
					ObjectNode reqNode = mapper.createObjectNode();
					ObjectNode rootNode = mapper.createObjectNode();
					ArrayNode arrayNode = mapper.createArrayNode();
				    rootNode.put("Warehouse", whse);
					rootNode.put("CustomerNumber", custno);
					rootNode.put("ShipTo", custno+shipTo);
					rootNode.put("SalesOrganization", cono);
					rootNode.put("DistributionChannel", "10");
					rootNode.put("ProductCode", pr);
					arrayNode.add(rootNode);
					reqNode.set("PriceBookEngineUpdateRequest", arrayNode);
					mainNode.set("PriceBookEngine", reqNode);
		
					String jsonString = mapper.writeValueAsString(mainNode);
					StringEntity reqEntity = new StringEntity(jsonString);
				    post.setEntity(reqEntity);
					
				    CloseableHttpResponse response = client.execute(post);
					if(response.getStatusLine().getStatusCode() == 200) {
						HttpEntity respEntity = response.getEntity();
					    if (respEntity != null) {
					    	String result = EntityUtils.toString(respEntity);
					    	/*if(result.contains("root")) {
					    		com.influx.bean.sapModel.PricingRoot resp = mapper.readValue(result, com.influx.bean.sapModel.PricingRoot.class);
					    		if(resp != null && resp.getRoot() != null && resp.getRoot().getRecordObject() != null 
					    				&& resp.getRoot().getRecordObject().getOutputPrice() != null && resp.getRoot().getRecordObject().getOutputPrice().getPrice() != null) {
					    			prod.setPrice(new BigDecimal(resp.getRoot().getRecordObject().getOutputPrice().getPrice()));
					    		} else {
					    			prod.setPrice(new BigDecimal(0));
					    		}
					    	} else {*/
					    		prod.setPrice(new BigDecimal(0));
							//}
					    }

					}
					products.add(prod);
		    	}
	        }

			client.close();	
			
	
        }catch(Exception ex) {
            log.error(ex.getMessage());
        }

        return products;
    }

    
    public Product getSAPProductPrice(Product prod)
    {

        ObjectMapper mapper = new ObjectMapper();
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		mapper.enable(DeserializationFeature.ACCEPT_SINGLE_VALUE_AS_ARRAY);

        try {
            
        	CloseableHttpClient client = getCloseableHttpClient();
			HttpPost post = new HttpPost(sapHost);
			String authInput = sapUser + ":" + sapPass ;
			String encodedString = Base64.getEncoder().encodeToString(authInput.getBytes());
			 
		    post.setHeader("Accept", "application/json");
		    post.setHeader("Content-type", "application/json");
		    post.setHeader("Authorization", "Basic "+encodedString);


			ObjectNode mainNode = mapper.createObjectNode();
			ObjectNode reqNode = mapper.createObjectNode();
			ObjectNode rootNode = mapper.createObjectNode();
			ArrayNode arrayNode = mapper.createArrayNode();
		    rootNode.put("Warehouse", prod.getPriceWhse());
			rootNode.put("CustomerNumber", prod.getCustNo());
			rootNode.put("ShipTo", prod.getCustNo() + prod.getShipTo());
			rootNode.put("SalesOrganization", prod.getCono());
			rootNode.put("DistributionChannel", "10");
			rootNode.put("ProductCode", prod.getProduct());
			arrayNode.add(rootNode);
			reqNode.set("PriceBookEngineUpdateRequest", arrayNode);
			mainNode.set("PriceBookEngine", reqNode);

			String jsonString = mapper.writeValueAsString(mainNode);
			log.info("Product Price Req: "+jsonString);
			StringEntity reqEntity = new StringEntity(jsonString);
		    post.setEntity(reqEntity);
			
		    CloseableHttpResponse response = client.execute(post);
			if(response.getStatusLine().getStatusCode() == 200) {
				HttpEntity respEntity = response.getEntity();
			    if (respEntity != null) {
			    	String result = EntityUtils.toString(respEntity);
			    	if(result.contains("root")) {
			    		com.influx.bean.sapModel.PricingRoot resp = mapper.readValue(result, com.influx.bean.sapModel.PricingRoot.class);
			    		if(resp != null && resp.getRoot() != null && resp.getRoot().getRecordObject() != null)
			    		{
			    			ArrayList<PricingRecord> recs = resp.getRoot().getRecordObject();
			    			if(recs != null && recs.size() > 0) {
			    				for(PricingRecord pr : recs) {
			    					if(pr.getOutputPrice() != null && pr.getOutputPrice().getPrice() != null) {
			    						prod.setPrice(new BigDecimal(pr.getOutputPrice().getPrice()));
			    					}
			    				}
			    			}
			    		}
			    	} else {
			    		prod.setPrice(new BigDecimal(0));
					}
			    }

			}

			client.close();	

        }catch(Exception ex) {
            log.error("Error while getting price in thread: "+ ex.getMessage());
        }

        return prod;
    }
    
    class productPriceCheck implements Runnable {
    	Product product = null;
		Vector<Boolean> temp = null;

		public productPriceCheck(Product product, Vector<Boolean> temp){
			this.product = product;
			this.temp = temp;
		}

		public void run() {
			getSAPProductPrice(product);
			temp.add(true);
		}
	}
    
    public List<Product> getSAPProducts(int cono, String oper, String custno, String shipTo, String whse, String[] prodArr) throws InterruptedException
    {
    	List<Product> list = new ArrayList<Product>();
    	Vector<Boolean> temp = new Vector<>();
    	
    	ThreadPoolExecutor executorPool = 
    			  (ThreadPoolExecutor) Executors.newFixedThreadPool(20);

    	for (String p : prodArr) {
	    	Product prod = new Product();
	    	prod.setProduct(p);
			prod.setAvail(BigDecimal.ZERO);
			prod.setPriceWhse(whse);
			prod.setCono(cono);
			prod.setCustNo(custno);
			prod.setShipTo(shipTo);
			executorPool.execute(new productPriceCheck(prod, temp));
			list.add(prod);
		}
    	
    	shutdownAndAwaitTermination(executorPool);
     
	    return list;
    }
    
    
    static void shutdownAndAwaitTermination(ExecutorService pool) {
  
        pool.shutdown();
        try {
          if (!pool.awaitTermination(60, TimeUnit.SECONDS)) {
            pool.shutdownNow();
            if (!pool.awaitTermination(60, TimeUnit.SECONDS))
              System.err.println("Pool did not terminate");
          }
        } catch (InterruptedException ex) {
        	pool.shutdownNow();
        	Thread.currentThread().interrupt();
        }
      }
    
    /**
     *
     */
    public String firstAvailWhse(int cono, String oper, String productCode)
    {
        SxAPIWorker sxapi = null;

        String whse = null;

        try {
            sxapi = new SxAPIWorker();

            ICProductAvailByWhseRequest req = new ICProductAvailByWhseRequest();
            req.setProductCode(productCode);

            ICProductAvailByWhse obj = new ICProductAvailByWhse();

            ICProductAvailByWhseResponse res = obj.prepareResults(sxapi.get(), "CONO=" + cono + "|OPER=" + oper, "", req);

            List<com.infor.sxapi2.paramclass.ICProductAvailByWhse.output.Availability> avail = res.getListAvailability();

            whse = avail.isEmpty() ? null : avail.get(0).getWarehouse();

        }catch(Exception ex) {
            log.error(ex.getMessage());
        }finally{
            sxapi.release();
        }

        return whse;
    }

    /**
     *
     */
    public String createWhseProduct(int cono, String oper, String prod, String srcWhse, String destWhse) throws Exception
    {
        SxAPIWorker sxapi = null;
        String results = null;

        try {
            sxapi = new SxAPIWorker();

            Map<String, String> keys = new HashMap<String, String>();

            //get general info about item
            ICGetWhseProductDataGeneralV2Request reqGen = new ICGetWhseProductDataGeneralV2Request();
            reqGen.setProduct(prod);
            reqGen.setWhse(srcWhse);

            ICGetWhseProductDataGeneralV2 objGen = new ICGetWhseProductDataGeneralV2();

            ICGetWhseProductDataGeneralV2Response resGen = objGen.prepareResults(sxapi.get(), "CONO=" + cono + "|OPER=" + oper, "", reqGen);

            keys.put("arptype", resGen.getARPType());
            keys.put("arpwhse", resGen.getARPWhse());
            keys.put("arpvendno", String.valueOf(resGen.getVendorNumber()));
            keys.put("pricetype", resGen.getPriceType());
            keys.put("prodline", resGen.getProductLine());
            keys.put("baseprice", String.valueOf(resGen.getBasePrice()));
            keys.put("listprice", String.valueOf(resGen.getListPrice()));
            keys.put("vendprod", resGen.getVendorProduct());
            keys.put("icswstatustype", "O");
            keys.put("serlottype", resGen.getSerialLotType());

            //get item costs
            ICGetWhseProductDataCostsRequest reqCost = new ICGetWhseProductDataCostsRequest();
            reqCost.setProduct(prod);
            reqCost.setWhse(srcWhse);

            ICGetWhseProductDataCosts objCost = new ICGetWhseProductDataCosts();

            ICGetWhseProductDataCostsResponse resCost = objCost.prepareResults(sxapi.get(), "CONO=" + cono + "|OPER=" + oper, "", reqCost);
            keys.put("stndcost", String.valueOf(resCost.getStandardCost()));
            keys.put("avgcost", String.valueOf(resCost.getAverageCost()));
            keys.put("replcost", String.valueOf(resCost.getReplacementCost()));
            keys.put("taxablety", "V");
            keys.put("taxgroup", "1");

            //create warehouse product
            ICProductMntRequest reqMnt = new ICProductMntRequest();

            int i = 1;
            List<FieldModification> fmList = new ArrayList<FieldModification>();
            Iterator it = keys.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pairs = (Map.Entry) it.next();

                FieldModification fm = new FieldModification();
                fm.setSetNumber(1);
                fm.setUpdateMode("add");
                fm.setKey1(prod);
                fm.setKey2(destWhse);
                fm.setSequenceNumber(i++);
                fm.setFieldName(pairs.getKey().toString());
                fm.setFieldValue(pairs.getValue().toString());

                fmList.add(fm);
            }
            reqMnt.setListFieldModification(fmList);

            ICProductMnt objMnt = new ICProductMnt();

            ICProductMntResponse resMnt = objMnt.prepareResults(sxapi.get(), "CONO=" + cono + "|OPER=" + oper, "", reqMnt);

            results = resMnt.getErrorMessage() + " " + resMnt.getReturnData();

        }catch(Exception ex) {
            log.error(ex.getMessage());
        }finally{
            sxapi.release();
        }

        return results;
    }

    /**
     *
     */
    public boolean isProductExist(int cono, String oper, String productCode, String whse)
    {
        SxAPIWorker sxapi = null;

        try {
            sxapi = new SxAPIWorker();

            ICProductAvailByWhseRequest req = new ICProductAvailByWhseRequest();
            req.setProductCode(productCode);

            ICProductAvailByWhse obj = new ICProductAvailByWhse();

            ICProductAvailByWhseResponse res = obj.prepareResults(sxapi.get(), "CONO=" + cono + "|OPER=" + oper, "", req);

            for(com.infor.sxapi2.paramclass.ICProductAvailByWhse.output.Availability avail : res.getListAvailability()) {
                if (avail.getWarehouse().equals(whse))
                    return true;
            }
        }catch(Exception ex) {
            log.error(ex.getMessage());
        }finally{
            sxapi.release();
        }

        return false;
    }
}