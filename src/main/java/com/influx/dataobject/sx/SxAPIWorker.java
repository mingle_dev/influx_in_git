/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.dataobject.sx;

import com.influx.ConfigReader;
import com.influx.Constants;
import com.infor.sxapi2.proxy.ApiAppObj;
import org.apache.log4j.Logger;

import java.io.Serializable;

/**
 *
 * @author chris.weaver
 */
public class SxAPIWorker implements Serializable
{
    /* SxAPI AppServer Connection Handler */
    private ApiAppObj apiAppObj;

    /* SxAPI Application Server */
    protected String apiAppSvrUrl = ConfigReader.getProperty("sxe.connectionString");

    /* SxAPI apiAppSvrRetryConnectTimes */
    private int apiAppSrvRetries = ConfigReader.getPropertyAsInt("sxe.appserverRetryConnectTimes");
    
    /* SxAPI apiAppSvrRetryConnectSleepSeconds */
    private int apiAppsrvSleep = ConfigReader.getPropertyAsInt("sxe.appserverRetryConnectSleepSeconds");
    
    /* SxAPI Connection identifier */
    protected String apiConnId;

    /* Logging */
    private static Logger log = Logger.getLogger( SxAPIWorker.class );

    public SxAPIWorker() {}
    
    public SxAPIWorker(String apiAppSvrUrl, int apiAppSrvRetries, int apiAppsrvSleep)
    {
        this.apiAppSvrUrl = apiAppSvrUrl;
        this.apiAppSrvRetries = apiAppSrvRetries;
        this.apiAppsrvSleep = apiAppsrvSleep;
    }

    /**
     * @return the sxapiWorker
     */
    protected ApiAppObj get()
    {
        try {
            if(apiAppObj == null)
                apiAppObj = new ApiAppObj(apiAppSvrUrl, "", "", "", apiAppSrvRetries, apiAppsrvSleep);
        }catch(Exception ex) {
            log.error("Couldn't create ApiAppObj for SxapiWorker.");
            log.error(apiAppSvrUrl);
            log.error(ex.getMessage());
            
            throw new RuntimeException("Couldn't create ApiAppObj for SxapiWorker", ex);
        }

        return apiAppObj;
    }
    
    protected void release()
    {
        //System.out.println("ApiAppObj:release()");
        
        try {
            if(apiAppObj != null)
                apiAppObj._release();
            
            apiAppObj = null;
            
        }catch(Exception ex) {
            log.error("Couldn't release ApiAppObj for SxapiWorker. " + ex);
        }
    }    
}