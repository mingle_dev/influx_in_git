package com.influx.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("Response")
public class BalanceResponse implements Serializable {

	private static final long serialVersionUID = 5926468583005150707L;
	
	@JsonProperty("CreditLimit")
    public String creditLimit;
	
	@JsonProperty("CreditExposure")
    public String creditExposure;
	
	@JsonProperty("BalanceLimit")
    public String balanceLimit;
	
	@JsonProperty("TotalDue")
    public String totalDue;
	
	@JsonProperty("FutureDue")
    public String futureDue;
	
	@JsonProperty("CurrentDue")
    public String currentDue;
	
	@JsonProperty("ThirtyDays")
    public String thirtyDays;
	
	@JsonProperty("SixtyDays")
    public String sixtyDays;
	
	@JsonProperty("NinetyDays")
    public String ninetyDays;
	
	@JsonProperty("HunderedandTwentyPlusDays")
    public String hunderedandTwentyPlusDays;
	
	@JsonProperty("LastPayment")
    public String lastPayment;
	
	@JsonProperty("SalesPrior")
    public String salesPrior;
	
	@JsonProperty("SalesCurrent")
    public String salesCurrent;
	
	@JsonProperty("OpenOrders")
    public String openOrders;
	
	@JsonProperty("ErrorMessage")
    public String errorMessage;
	

	private String FutureInvoiceBalance;
	
	@JsonProperty("CODBalance")
	private String codBalance;
	
	@JsonProperty("ServiceCharges")
    private String serviceCharges;
	
	@JsonProperty("MiscCredit")
	private String miscCredit;
	
	@JsonProperty("UnAppliedCash")
	private String unAppliedCash;
	
	@JsonProperty("LastPurchaseDate")
	private String lastPurchaseDate;
	
	@JsonProperty("DownPayment")
	private String downPayment;
	
	@JsonProperty("LastPurchaseAmt")
	private String lastPurchaseAmt;
	
	@JsonProperty("LastPaymentDate")
	private String lastPaymentDate;
	
	public String getLastPurchaseDate() {
		return lastPurchaseDate;
	}

	public void setLastPurchaseDate(String lastPurchaseDate) {
		this.lastPurchaseDate = lastPurchaseDate;
	}

	public String getFutureInvoiceBalance() {
		return FutureInvoiceBalance;
	}

	public void setFutureInvoiceBalance(String futureInvoiceBalance) {
		FutureInvoiceBalance = futureInvoiceBalance;
	}


	public String getCodBalance() {
		return codBalance;
	}

	public void setCodBalance(String codBalance) {
		this.codBalance = codBalance;
	}

	public String getServiceCharges() {
		return serviceCharges;
	}

	public void setServiceCharges(String serviceCharges) {
		this.serviceCharges = serviceCharges;
	}

	public String getMiscCredit() {
		return miscCredit;
	}

	public void setMiscCredit(String miscCredit) {
		this.miscCredit = miscCredit;
	}

	public String getUnAppliedCash() {
		return unAppliedCash;
	}

	public void setUnAppliedCash(String unAppliedCash) {
		this.unAppliedCash = unAppliedCash;
	}

	public String getCreditLimit() {
		return creditLimit;
	}

	public void setCreditLimit(String creditLimit) {
		this.creditLimit = creditLimit;
	}

	public String getCreditExposure() {
		return creditExposure;
	}

	public void setCreditExposure(String creditExposure) {
		this.creditExposure = creditExposure;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public String getBalanceLimit() {
		return balanceLimit;
	}

	public void setBalanceLimit(String balanceLimit) {
		this.balanceLimit = balanceLimit;
	}

	public String getTotalDue() {
		return totalDue;
	}

	public void setTotalDue(String totalDue) {
		this.totalDue = totalDue;
	}

	public String getFutureDue() {
		return futureDue;
	}

	public void setFutureDue(String futureDue) {
		this.futureDue = futureDue;
	}

	public String getCurrentDue() {
		return currentDue;
	}

	public void setCurrentDue(String currentDue) {
		this.currentDue = currentDue;
	}

	public String getThirtyDays() {
		return thirtyDays;
	}

	public void setThirtyDays(String thirtyDays) {
		this.thirtyDays = thirtyDays;
	}

	public String getSixtyDays() {
		return sixtyDays;
	}

	public void setSixtyDays(String sixtyDays) {
		this.sixtyDays = sixtyDays;
	}

	public String getNinetyDays() {
		return ninetyDays;
	}

	public void setNinetyDays(String ninetyDays) {
		this.ninetyDays = ninetyDays;
	}

	public String getHunderedandTwentyPlusDays() {
		return hunderedandTwentyPlusDays;
	}

	public void setHunderedandTwentyPlusDays(String hunderedandTwentyPlusDays) {
		this.hunderedandTwentyPlusDays = hunderedandTwentyPlusDays;
	}

	public String getLastPayment() {
		return lastPayment;
	}

	public void setLastPayment(String lastPayment) {
		this.lastPayment = lastPayment;
	}

	public String getSalesPrior() {
		return salesPrior;
	}

	public void setSalesPrior(String salesPrior) {
		this.salesPrior = salesPrior;
	}

	public String getSalesCurrent() {
		return salesCurrent;
	}

	public void setSalesCurrent(String salesCurrent) {
		this.salesCurrent = salesCurrent;
	}

	public String getOpenOrders() {
		return openOrders;
	}

	public void setOpenOrders(String openOrders) {
		this.openOrders = openOrders;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getDownPayment() {
		return downPayment;
	}

	public void setDownPayment(String downPayment) {
		this.downPayment = downPayment;
	}

	public String getLastPurchaseAmt() {
		return lastPurchaseAmt;
	}

	public void setLastPurchaseAmt(String lastPurchaseAmt) {
		this.lastPurchaseAmt = lastPurchaseAmt;
	}

	public String getLastPaymentDate() {
		return lastPaymentDate;
	}

	public void setLastPaymentDate(String lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}

	@Override
	public String toString() {
		return "BalanceResponse [creditLimit=" + creditLimit + ", creditExposure=" + creditExposure + ", balanceLimit="
				+ balanceLimit + ", totalDue=" + totalDue + ", futureDue=" + futureDue + ", currentDue=" + currentDue
				+ ", thirtyDays=" + thirtyDays + ", sixtyDays=" + sixtyDays + ", ninetyDays=" + ninetyDays
				+ ", hunderedandTwentyPlusDays=" + hunderedandTwentyPlusDays + ", lastPayment=" + lastPayment
				+ ", salesPrior=" + salesPrior + ", salesCurrent=" + salesCurrent + ", openOrders=" + openOrders
				+ ", errorMessage=" + errorMessage + ", FutureInvoiceBalance=" + FutureInvoiceBalance + ", codBalance="
				+ codBalance + ", serviceCharges=" + serviceCharges + ", miscCredit=" + miscCredit + ", unAppliedCash="
				+ unAppliedCash + ", lastPurchaseDate=" + lastPurchaseDate + ", downPayment=" + downPayment
				+ ", lastPurchaseAmt=" + lastPurchaseAmt + ", lastPaymentDate=" + lastPaymentDate + "]";
	}
}
