package com.influx.bean;

public class InsiteCartLine {
	
	private String sku;
    private int qty;
    private String status;
    
	public InsiteCartLine(String sku, int qty) {
		super();
		this.sku = sku;
		this.qty = qty;
	}
	
	public InsiteCartLine() {
	}
	
	public String getSku() {
		return sku;
	}
	public void setSku(String sku) {
		this.sku = sku;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	
}
