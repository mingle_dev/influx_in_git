package com.influx.bean;

import java.io.Serializable;

/**
 * @author Nara
 */
public class PurchaseData implements Serializable
{
	private LineItems lineItems;
    private String localTaxAmount;
    private String customerCode;
    private String nationalTaxAmount;
    private String summaryCommodityCode;
    private String freightAmount;
    private String dutyAmount;
    private String destinationPostal;
    private String shipFromPostal;
    private String destinationCountry;
    private String vATInvoiceReferenceId;
    private String vATTaxAmount;
    private String vATTaxRate;
    private String taxExempt;
    
	
	/**
	 * @return the lineItemList
	 */
	public LineItems getLineItems() {
		return lineItems;
	}
	/**
	 * @param lineItemList the lineItemList to set
	 */
	public void setLineItems(LineItems lineItems) {
		this.lineItems = lineItems;
	}
	/**
	 * @return the localTaxAmount
	 */
	public String getLocalTaxAmount() {
		return localTaxAmount;
	}
	/**
	 * @param localTaxAmount the localTaxAmount to set
	 */
	public void setLocalTaxAmount(String localTaxAmount) {
		this.localTaxAmount = localTaxAmount;
	}
	/**
	 * @return the customerCode
	 */
	public String getCustomerCode() {
		return customerCode;
	}
	/**
	 * @param customerCode the customerCode to set
	 */
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	/**
	 * @return the nationalTaxAmount
	 */
	public String getNationalTaxAmount() {
		return nationalTaxAmount;
	}
	/**
	 * @param nationalTaxAmount the nationalTaxAmount to set
	 */
	public void setNationalTaxAmount(String nationalTaxAmount) {
		this.nationalTaxAmount = nationalTaxAmount;
	}
	/**
	 * @return the summaryCommodityCode
	 */
	public String getSummaryCommodityCode() {
		return summaryCommodityCode;
	}
	/**
	 * @param summaryCommodityCode the summaryCommodityCode to set
	 */
	public void setSummaryCommodityCode(String summaryCommodityCode) {
		this.summaryCommodityCode = summaryCommodityCode;
	}
	/**
	 * @return the freightAmount
	 */
	public String getFreightAmount() {
		return freightAmount;
	}
	/**
	 * @param freightAmount the freightAmount to set
	 */
	public void setFreightAmount(String freightAmount) {
		this.freightAmount = freightAmount;
	}
	/**
	 * @return the dutyAmount
	 */
	public String getDutyAmount() {
		return dutyAmount;
	}
	/**
	 * @param dutyAmount the dutyAmount to set
	 */
	public void setDutyAmount(String dutyAmount) {
		this.dutyAmount = dutyAmount;
	}
	/**
	 * @return the destinationPostal
	 */
	public String getDestinationPostal() {
		return destinationPostal;
	}
	/**
	 * @param destinationPostal the destinationPostal to set
	 */
	public void setDestinationPostal(String destinationPostal) {
		this.destinationPostal = destinationPostal;
	}
	/**
	 * @return the shipFromPostal
	 */
	public String getShipFromPostal() {
		return shipFromPostal;
	}
	/**
	 * @param shipFromPostal the shipFromPostal to set
	 */
	public void setShipFromPostal(String shipFromPostal) {
		this.shipFromPostal = shipFromPostal;
	}
	/**
	 * @return the destinationCountry
	 */
	public String getDestinationCountry() {
		return destinationCountry;
	}
	/**
	 * @param destinationCountry the destinationCountry to set
	 */
	public void setDestinationCountry(String destinationCountry) {
		this.destinationCountry = destinationCountry;
	}
	/**
	 * @return the vATInvoiceReferenceId
	 */
	public String getvATInvoiceReferenceId() {
		return vATInvoiceReferenceId;
	}
	/**
	 * @param vATInvoiceReferenceId the vATInvoiceReferenceId to set
	 */
	public void setvATInvoiceReferenceId(String vATInvoiceReferenceId) {
		this.vATInvoiceReferenceId = vATInvoiceReferenceId;
	}
	/**
	 * @return the vATTaxAmount
	 */
	public String getvATTaxAmount() {
		return vATTaxAmount;
	}
	/**
	 * @param vATTaxAmount the vATTaxAmount to set
	 */
	public void setvATTaxAmount(String vATTaxAmount) {
		this.vATTaxAmount = vATTaxAmount;
	}
	/**
	 * @return the vATTaxRate
	 */
	public String getvATTaxRate() {
		return vATTaxRate;
	}
	/**
	 * @param vATTaxRate the vATTaxRate to set
	 */
	public void setvATTaxRate(String vATTaxRate) {
		this.vATTaxRate = vATTaxRate;
	}
	/**
	 * @return the taxExempt
	 */
	public String getTaxExempt() {
		return taxExempt;
	}
	/**
	 * @param taxExempt the taxExempt to set
	 */
	public void setTaxExempt(String taxExempt) {
		this.taxExempt = taxExempt;
	}
    
}