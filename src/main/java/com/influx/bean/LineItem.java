package com.influx.bean;

import java.io.Serializable;


/**
 *
 * @author Nara
 */
public class LineItem implements Serializable
{
   
    private String CommodityCode;
    private String Description;
    private String ProductCode;
    private String Quantity;
    private String UnitOfMeasure;
    private String UnitAmount;
    private String TaxAmount;
    private String TaxRate;
    private String DiscountAmount;
    
	/**
	 * @return the commodityCode
	 */
	public String getCommodityCode() {
		return CommodityCode;
	}
	/**
	 * @param commodityCode the commodityCode to set
	 */
	public void setCommodityCode(String commodityCode) {
		CommodityCode = commodityCode;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return Description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		Description = description;
	}
	/**
	 * @return the productCode
	 */
	public String getProductCode() {
		return ProductCode;
	}
	/**
	 * @param productCode the productCode to set
	 */
	public void setProductCode(String productCode) {
		ProductCode = productCode;
	}
	/**
	 * @return the quantity
	 */
	public String getQuantity() {
		return Quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(String quantity) {
		Quantity = quantity;
	}
	/**
	 * @return the unitOfMeasure
	 */
	public String getUnitOfMeasure() {
		return UnitOfMeasure;
	}
	/**
	 * @param unitOfMeasure the unitOfMeasure to set
	 */
	public void setUnitOfMeasure(String unitOfMeasure) {
		UnitOfMeasure = unitOfMeasure;
	}
	/**
	 * @return the unitAmount
	 */
	public String getUnitAmount() {
		return UnitAmount;
	}
	/**
	 * @param unitAmount the unitAmount to set
	 */
	public void setUnitAmount(String unitAmount) {
		UnitAmount = unitAmount;
	}
	/**
	 * @return the taxAmount
	 */
	public String getTaxAmount() {
		return TaxAmount;
	}
	/**
	 * @param taxAmount the taxAmount to set
	 */
	public void setTaxAmount(String taxAmount) {
		TaxAmount = taxAmount;
	}
	/**
	 * @return the taxRate
	 */
	public String getTaxRate() {
		return TaxRate;
	}
	/**
	 * @param taxRate the taxRate to set
	 */
	public void setTaxRate(String taxRate) {
		TaxRate = taxRate;
	}
	/**
	 * @return the discountAmount
	 */
	public String getDiscountAmount() {
		return DiscountAmount;
	}
	/**
	 * @param discountAmount the discountAmount to set
	 */
	public void setDiscountAmount(String discountAmount) {
		DiscountAmount = discountAmount;
	}
    
}