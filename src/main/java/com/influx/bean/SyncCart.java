/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.bean;

import java.io.Serializable;
import java.util.ArrayList;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 *
 * @author chris
 */
@Document
@XmlAccessorType(XmlAccessType.NONE)
public class SyncCart implements Serializable
{
    @Id
    private String id;
    private ArrayList<SyncCartItem> items;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ArrayList<SyncCartItem> getItems() {
        return items;
    }

    @XmlElement(name="item", required=true, nillable=false)
    public void setItems(ArrayList<SyncCartItem> items) {
        this.items = items;
    }
}