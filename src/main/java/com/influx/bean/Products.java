package com.influx.bean;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import java.util.ArrayList;

/**
 * Created by chris on 2/9/15.
 */
public class Products
{
    private ArrayList<Product> productList = new ArrayList<Product>();

    public void addProduct(Product product) {
        productList.add(product);
    }

    public void setProductList(ArrayList<Product> productList) {
        this.productList = productList;
    }

    @XmlElement(name="product")
    public ArrayList<Product> getProductList() {
        return productList;
    }
}
