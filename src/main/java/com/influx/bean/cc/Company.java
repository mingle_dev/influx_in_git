package com.influx.bean.cc;

import java.util.ArrayList;
import java.util.List;

public class Company
{
    private int cono;
    private List<String> whses;

    //default constructor
    public Company(){}

    /**
     * @return the cono
     */
    public int getCono() {
        return cono;
    }

    /**
     * @param cono the cono to set
     */
    public void setCono(int cono) {
        this.cono = cono;
    }

    /**
     * @return the whses
     */
    public List<String> getWhses() {
        return whses;
    }

    /**
     * @param whses the whses to set
     */
    public void setWhses(List<String> whses) {
        this.whses = whses;
    }

    public void addWhse(String whse) {
        if(whses == null)
            whses = new ArrayList<String>();

        whses.add(whse);
    }
}