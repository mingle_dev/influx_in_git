package com.influx.bean.cc;

public class CardValidation
{
    /**
     * Perform Mod-10 (LUHN algorithm) on credit card # to determine if valid
     *
     * @param cardno
     * @return
     */
    public static boolean luhnValid(String cardno)
    {
        //cardno = "4012888888881881"; //valid
        //cardno = "4012888888832881"; //invalid

        int[] cc = new int[16];
        int ccLen = 0;

        try {
            char[] t = cardno.toCharArray();
            for(int i=0; i<t.length; i++) {
                cc[i] = t[i] - 48;
                ccLen++;
                if(cc[i] < 0 || cc[i] > 9)
                    return false;
            }
        }catch(Exception ignore) {
            return false;
        }

        int val = 0;
        int sum = 0;
        int weight = 1;

        for(int i=(ccLen-1); i>=0; i--) {
            val = cc[i] * weight;
            if(val > 9)
                val -= 9;
            sum += val;
            weight = (weight == 1) ? 2 : 1;
        }

        return ((sum % 10) == 0);
    }
}