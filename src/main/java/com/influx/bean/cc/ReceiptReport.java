package com.influx.bean.cc;

import java.math.BigDecimal;

public class ReceiptReport
{
    private String companyNumber, warehouse;
    private int totalTrxs, oldTrxs, newTrxs;
    private String whseAddress;
    private String transDate;

    private BigDecimal totalAmt = new BigDecimal(0);
    
    public String getCompanyNumber() {
		return companyNumber;
	}

	public void setCompanyNumber(String companyNumber) {
		this.companyNumber = companyNumber;
	}

	public String getWarehouse() {
		return warehouse;
	}

	public void setWarehouse(String warehouse) {
		this.warehouse = warehouse;
	}

	public int getOldTrxs() {
		return oldTrxs;
	}

	public void setOldTrxs(int oldTrxs) {
		this.oldTrxs = oldTrxs;
	}

	public int getNewTrxs() {
		return newTrxs;
	}

	public void setNewTrxs(int newTrxs) {
		this.newTrxs = newTrxs;
	}

	public int getTotalTrxs() {
		return totalTrxs;
	}

	public void setTotalTrxs(int totalTrxs) {
		this.totalTrxs = totalTrxs;
	}

	public BigDecimal getTotalAmt() {
		return totalAmt;
	}

	public void setTotalAmt(BigDecimal totalAmt) {
		this.totalAmt = totalAmt;
	}

	public String getWhseAddress() {
		return whseAddress;
	}

	public void setWhseAddress(String whseAddress) {
		this.whseAddress = whseAddress;
	}

	/**
     * @return the transDate
     */
    public String getTransDate() {
        return transDate;
    }
    
    public void setTransDate(String transDate) {
		this.transDate = transDate;
	}
    
    public void foundOneReceipt(CardReceipt r) {
    	totalTrxs++;
    	if(r.getCardNumber().startsWith("***"))
    		newTrxs++;
    	else
    		oldTrxs++;
    	
    	BigDecimal validAmount = new BigDecimal(r.getTransAmount());
    	totalAmt = totalAmt.add(validAmount);
    }
}