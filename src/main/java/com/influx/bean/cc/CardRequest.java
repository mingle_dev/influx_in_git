package com.influx.bean.cc;

public class CardRequest
{
    private int cono;
    private String orderno, whse;
    private String trackData, cardno, cardexpdt, cardcvv2, cardname, amount;

    private String address, city, state, zip;
    private String phoneno, emailaddr, phoneAuthCode;

    /**
     * @return the cono
     */
    public int getCono() {
        return cono;
    }

    /**
     * @param cono the cono to set
     */
    public void setCono(int cono) {
        this.cono = cono;
    }

    /**
     * @return the orderno
     */
    public String getOrderno() {
        return orderno;
    }

    /**
     * @param orderno the orderno to set
     */
    public void setOrderno(String orderno) {
        this.orderno = orderno;
    }

    /**
     * @return the whse
     */
    public String getWhse() {
        return whse;
    }

    /**
     * @param whse the whse to set
     */
    public void setWhse(String whse) {
        this.whse = whse;
    }

    /**
     * @return the cardno
     */
    public String getCardno() {
        return cardno;
    }

    /**
     * @param cardno the cardno to set
     */
    public void setCardno(String cardno) {
        this.cardno = cardno;
    }

    /**
     * @return the cardexpdt
     */
    public String getCardexpdt() {
        return cardexpdt;
    }

    /**
     * @param cardexpdt the cardexpdt to set
     */
    public void setCardexpdt(String cardexpdt) {
        this.cardexpdt = cardexpdt;
    }

    /**
     * @return the cardcvv2
     */
    public String getCardcvv2() {
        return cardcvv2;
    }

    /**
     * @param cardcvv2 the cardcvv2 to set
     */
    public void setCardcvv2(String cardcvv2) {
        this.cardcvv2 = cardcvv2;
    }

    /**
     * @return the cardname
     */
    public String getCardname() {
        return cardname;
    }

    /**
     * @param cardname the cardname to set
     */
    public void setCardname(String cardname) {
        this.cardname = cardname;
    }

    /**
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount the amount to set
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the zip
     */
    public String getZip() {
        return zip;
    }

    /**
     * @param zip the zip to set
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     * @return the phoneno
     */
    public String getPhoneno() {
        return phoneno;
    }

    /**
     * @param phoneno the phoneno to set
     */
    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    /**
     * @return the emailaddr
     */
    public String getEmailaddr() {
        return emailaddr;
    }

    /**
     * @param emailaddr the emailaddr to set
     */
    public void setEmailaddr(String emailaddr) {
        this.emailaddr = emailaddr;
    }

    /**
     * @return the emailaddr
     */
    public String getPhoneAuthCode() {
        return phoneAuthCode;
    }

    /**
     * @param emailaddr the emailaddr to set
     */
    public void setPhoneAuthCode(String phoneAuthCode) {
        this.phoneAuthCode = phoneAuthCode;
    }

    /**
     * @return the trackData
     */
    public String getTrackData() {
        return trackData;
    }

    /**
     * @param trackData the trackData to set
     */
    public void setTrackData(String trackData) {
        this.trackData = trackData;
    }
}
