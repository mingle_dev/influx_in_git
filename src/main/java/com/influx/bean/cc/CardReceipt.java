package com.influx.bean.cc;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CardReceipt
{
    private String companyNumber, warehouse, orderNo;
    private String cardName, cardNumber, cardType;
    private String cardExpMonth, cardExpYear, cardExpDate, transType, transAmount;
    private String billAddress, billCity, billState, billZip;
    private String phone, email;
    private String transId, authCode, responseCode, responseMessage;
    private String whseAddress;

    private String transDate;

    public CardReceipt()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyMMdd");
        Date date = new Date();
        transDate = dateFormat.format(date);
    }

    public String getWhseAddress() {
		return whseAddress;
	}

	public void setWhseAddress(String whseAddress) {
		this.whseAddress = whseAddress;
	}

	/**
     * @return the companyNumber
     */
    public String getCompanyNumber() {
        return companyNumber;
    }

    /**
     * @param companyNumber the companyNumber to set
     */
    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }

    /**
     * @return the warehouse
     */
    public String getWarehouse() {
        return warehouse;
    }

    /**
     * @param warehouse the warehouse to set
     */
    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    /**
     * @return the orderNo
     */
    public String getOrderNo() {
        return orderNo;
    }

    /**
     * @param orderNo the orderNo to set
     */
    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    /**
     * @return the cardName
     */
    public String getCardName() {
        return cardName;
    }

    /**
     * @param cardName the cardName to set
     */
    public void setCardName(String cardName) {
        this.cardName = cardName;
    }

    /**
     * @return the cardNumber
     */
    public String getCardNumber() {
        return cardNumber;
    }

    /**
     * @param cardNumber the cardNumber to set
     */
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    /**
     * @return the cardType
     */
    public String getCardType() {
        return cardType;
    }

    /**
     * @param cardType the cardType to set
     */
    public void setCardType(String cardType) {
        this.cardType = cardType;
    }

    /**
     * @return the cardExpMonth
     */
    public String getCardExpDate() {
        return cardExpDate;
    }

    /**
     * @param cardExpMonth the cardExpMonth to set
     */
    public void setCardExpDate(String cardExpDate) {
        this.cardExpDate = cardExpDate;
    }

    /**
     *
     * @return the cardExpMonth
     */
    public String getCardExpMonth() {
        return cardExpDate.substring(0,2);
    }

    /**
     *
     * @return the cardExpYear
     */
    public String getCardExpYear() {
        return cardExpDate.substring(3,4);
    }

    /**
     * @return the transType
     */
    public String getTransType() {
        return transType;
    }

    /**
     * @param transType the transType to set
     */
    public void setTransType(String transType) {
        this.transType = transType;
    }

    /**
     * @return the transAmount
     */
    public String getTransAmount() {
        return transAmount;
    }

    /**
     * @param transAmount the transAmount to set
     */
    public void setTransAmount(String transAmount) {
        this.transAmount = transAmount;
    }

    /**
     * @return the billAddress
     */
    public String getBillAddress() {
        return billAddress;
    }

    /**
     * @param billAddress the billAddress to set
     */
    public void setBillAddress(String billAddress) {
        this.billAddress = billAddress;
    }

    /**
     * @return the billCity
     */
    public String getBillCity() {
        return billCity;
    }

    /**
     * @param billCity the billCity to set
     */
    public void setBillCity(String billCity) {
        this.billCity = billCity;
    }

    /**
     * @return the billState
     */
    public String getBillState() {
        return billState;
    }

    /**
     * @param billState the billState to set
     */
    public void setBillState(String billState) {
        this.billState = billState;
    }

    /**
     * @return the billZip
     */
    public String getBillZip() {
        return billZip;
    }

    /**
     * @param billZip the billZip to set
     */
    public void setBillZip(String billZip) {
        this.billZip = billZip;
    }

    /**
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * @param phone the phone to set
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the transId
     */
    public String getTransId() {
        return transId;
    }

    /**
     * @param transId the transId to set
     */
    public void setTransId(String transId) {
        this.transId = transId;
    }

    /**
     * @return the authCode
     */
    public String getAuthCode() {
        return authCode;
    }

    /**
     * @param authCode the authCode to set
     */
    public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }

    /**
     * @return the responseCode
     */
    public String getResponseCode() {
        return responseCode;
    }

    /**
     * @param responseCode the responseCode to set
     */
    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    /**
     * @return the responseMessage
     */
    public String getResponseMessage() {
        return responseMessage;
    }

    /**
     * @param responseMessage the responseMessage to set
     */
    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    /**
     * @return the transDate
     */
    public String getTransDate() {
        return transDate;
    }
    
    public void setTransDate(String transDate) {
		this.transDate = transDate;
	}

	public CardReceipt(String companyNumber, String warehouse, String orderNo, String cardName, String cardNumber,
			String cardType, String cardExpMonth, String cardExpYear, String cardExpDate, String transType,
			String transAmount, String billAddress, String billCity, String billState, String billZip, String phone,
			String email, String transId, String authCode, String responseCode, String responseMessage,
			String whseAddress, String transDate) {
		this.companyNumber = companyNumber;
		this.warehouse = warehouse;
		this.orderNo = orderNo;
		this.cardName = cardName;
		this.cardNumber = cardNumber;
		this.cardType = cardType;
		this.cardExpMonth = cardExpMonth;
		this.cardExpYear = cardExpYear;
		this.cardExpDate = cardExpDate;
		this.transType = transType;
		this.transAmount = transAmount;
		this.billAddress = billAddress;
		this.billCity = billCity;
		this.billState = billState;
		this.billZip = billZip;
		this.phone = phone;
		this.email = email;
		this.transId = transId;
		this.authCode = authCode;
		this.responseCode = responseCode;
		this.responseMessage = responseMessage;
		this.whseAddress = whseAddress;
		this.transDate = transDate;
	}
    
    
    
}