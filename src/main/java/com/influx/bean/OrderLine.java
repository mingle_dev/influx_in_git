/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.influx.bean;

import java.io.Serializable;
import java.math.BigDecimal;


/**
 *
 * @author chris.weaver
 */
public class OrderLine implements Serializable
{
    private String id;
    private int lineNumber;
    private String shipProd;
    private BigDecimal qtyOrder;
    private BigDecimal qtyShip;
    private String description; //<-- this comes from icsp
    private BigDecimal price;
    private BigDecimal netAmount;
    private Order order;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the shipProd
     */
    public String getShipProd() {
        return shipProd;
    }

    /**
     * @param shipProd the shipProd to set
     */
    public void setShipProd(String shipProd) {
        this.shipProd = shipProd;
    }

    /**
     * @return the order
     */
    public Order getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(Order order) {
        this.order = order;
    }

    /**
     * @return the qtyOrder
     */
    public BigDecimal getQtyOrder() {
        return qtyOrder;
    }

    /**
     * @param qtyOrder the qtyOrder to set
     */
    public void setQtyOrder(BigDecimal qtyOrder) {
        this.qtyOrder = qtyOrder;
    }

    /**
     * @return the qtyShip
     */
    public BigDecimal getQtyShip() {
        return qtyShip;
    }

    /**
     * @param qtyShip the qtyShip to set
     */
    public void setQtyShip(BigDecimal qtyShip) {
        this.qtyShip = qtyShip;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the price
     */
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    /**
     * @return the netAmount
     */
    public BigDecimal getNetAmount() {
        return netAmount;
    }

    /**
     * @param netAmount the netAmount to set
     */
    public void setNetAmount(BigDecimal netAmount) {
        this.netAmount = netAmount;
    }

    /**
     * @return the lineNumber
     */
    public int getLineNumber() {
        return lineNumber;
    }

    /**
     * @param lineNumber the lineNumber to set
     */
    public void setLineNumber(int lineNumber) {
        this.lineNumber = lineNumber;
    }
}
