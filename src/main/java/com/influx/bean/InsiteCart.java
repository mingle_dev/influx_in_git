package com.influx.bean;

import java.util.List;

public class InsiteCart {
	
	private String email;
    private List<InsiteCartLine> products;
    
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<InsiteCartLine> getProducts() {
		return products;
	}
	public void setProducts(List<InsiteCartLine> products) {
		this.products = products;
	}
	
    
}
