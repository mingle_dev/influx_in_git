package com.influx.bean.sapModel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;


@JsonRootName("ProductPricing")
public class ProductPricing implements Serializable {

	private static final long serialVersionUID = 5926468583005150707L;

	
	private String price;
	private String matnr;
	private String targetQty;
	private String werks;

	@JsonProperty("price")
	public String getPrice() {
		return price;
	}

	@JsonProperty("COND_VALUE")
	public void setPrice(String price) {
		this.price = price;
	}

	@JsonProperty("matnr")
	public String getMatnr() {
		return matnr;
	}

	@JsonProperty("MATNR")
	public void setMatnr(String matnr) {
		this.matnr = matnr;
	}

	@JsonProperty("targetQty")
	public String getTargetQty() {
		return targetQty;
	}

	@JsonProperty("TARGET_QTY")
	public void setTargetQty(String targetQty) {
		this.targetQty = targetQty;
	}

	@JsonProperty("werks")
	public String getWerks() {
		return werks;
	}

	@JsonProperty("WERKS")
	public void setWerks(String werks) {
		this.werks = werks;
	}

}