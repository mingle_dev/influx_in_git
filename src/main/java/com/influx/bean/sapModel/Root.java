package com.influx.bean.sapModel;

import java.io.Serializable;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Root implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@JsonProperty("Record") 
	ArrayList<PricingRecord> RecordObject;

	public ArrayList<PricingRecord> getRecordObject() {
		return RecordObject;
	}

	public void setRecordObject(ArrayList<PricingRecord> recordObject) {
		RecordObject = recordObject;
	}

	

 	
}