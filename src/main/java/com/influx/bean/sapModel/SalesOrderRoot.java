
package com.influx.bean.sapModel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "SALES_ORDER_DETAILSSet"
})
public class SalesOrderRoot {

    @JsonProperty("SALES_ORDER_DETAILSSet")
    private SALESORDERDETAILSSet sALESORDERDETAILSSet;


    @JsonProperty("SALES_ORDER_DETAILSSet")
    public SALESORDERDETAILSSet getSALESORDERDETAILSSet() {
        return sALESORDERDETAILSSet;
    }

    @JsonProperty("SALES_ORDER_DETAILSSet")
    public void setSALESORDERDETAILSSet(SALESORDERDETAILSSet sALESORDERDETAILSSet) {
        this.sALESORDERDETAILSSet = sALESORDERDETAILSSet;
    }



}
