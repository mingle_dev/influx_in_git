
package com.influx.bean.sapModel;

import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "Vbeln",
    "Plant",
    "NET_PRICE",
    "ShortText",
    "ReqQty",
    "DlvQty",
    "ITM_NUMBER",
    "MATERIAL"
})
public class SalesOrderItem {

    @JsonProperty("Vbeln")
    private String vbeln;
    @JsonProperty("Plant")
    private String plant;
    @JsonProperty("NET_PRICE")
    private String netPrice;
    @JsonProperty("ShortText")
    private String shortText;
    @JsonProperty("ReqQty")
    private String reqQty;
    @JsonProperty("DlvQty")
    private String dlvQty;
    @JsonProperty("ITM_NUMBER")
    private String itmNumber;
    @JsonProperty("MATERIAL")
    private String material;


    @JsonProperty("Vbeln")
    public String getVbeln() {
        return vbeln;
    }

    @JsonProperty("Vbeln")
    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    @JsonProperty("Plant")
    public String getPlant() {
        return plant;
    }

    @JsonProperty("Plant")
    public void setPlant(String plant) {
        this.plant = plant;
    }

    @JsonProperty("NET_PRICE")
    public String getNetPrice() {
        return netPrice;
    }

    @JsonProperty("NET_PRICE")
    public void setNetPrice(String netPrice) {
        this.netPrice = netPrice;
    }

    @JsonProperty("ShortText")
    public String getShortText() {
        return shortText;
    }

    @JsonProperty("ShortText")
    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    @JsonProperty("ReqQty")
    public String getReqQty() {
        return reqQty;
    }

    @JsonProperty("ReqQty")
    public void setReqQty(String reqQty) {
        this.reqQty = reqQty;
    }

    @JsonProperty("DlvQty")
    public String getDlvQty() {
        return dlvQty;
    }

    @JsonProperty("DlvQty")
    public void setDlvQty(String dlvQty) {
        this.dlvQty = dlvQty;
    }

    @JsonProperty("ITM_NUMBER")
    public String getItmNumber() {
        return itmNumber;
    }

    @JsonProperty("ITM_NUMBER")
    public void setItmNumber(String itmNumber) {
        this.itmNumber = itmNumber;
    }

    @JsonProperty("MATERIAL")
    public String getMaterial() {
        return material;
    }

    @JsonProperty("MATERIAL")
    public void setMaterial(String material) {
        this.material = material;
    }


}
