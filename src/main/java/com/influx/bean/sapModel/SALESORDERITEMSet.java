
package com.influx.bean.sapModel;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "SALES_ORDER_ITEM"
})
public class SALESORDERITEMSet {

	
	@JsonProperty("SALES_ORDER_ITEM")
	//@JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
	private List<SalesOrderItem> salesOrderItems = null;
	
/*    @JsonProperty("SALES_ORDER_ITEM")
    private SalesOrderItem salesOrderItem;*/

    @JsonProperty("SALES_ORDER_ITEM")
    public List<SalesOrderItem> getSalesOrderItems() {
    return salesOrderItems;
    }

    @JsonProperty("SALES_ORDER_ITEM")
    public void setSalesOrderItems(List<SalesOrderItem> salesOrderItems) {
    this.salesOrderItems = salesOrderItems;
    }

  /*  @JsonProperty("SALES_ORDER_ITEM")
    public SalesOrderItem getSalesOrderItem() {
        return salesOrderItem;
    }

    @JsonProperty("SALES_ORDER_ITEM")
    public void setSalesOrderItem(SalesOrderItem salesOrderItem) {
        this.salesOrderItem = salesOrderItem;
    }*/


}
