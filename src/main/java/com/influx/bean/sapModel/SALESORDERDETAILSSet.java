
package com.influx.bean.sapModel;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "SALES_ORDER_DETAILS"
})
public class SALESORDERDETAILSSet {

    @JsonProperty("SALES_ORDER_DETAILS")
    private SalesOrderDetails salesOrderDetails;


    @JsonProperty("SALES_ORDER_DETAILS")
    public SalesOrderDetails getSalesOrderDetails() {
        return salesOrderDetails;
    }

    @JsonProperty("SALES_ORDER_DETAILS")
    public void setSalesOrderDetails(SalesOrderDetails salesOrderDetails) {
        this.salesOrderDetails = salesOrderDetails;
    }



}
