package com.influx.bean.sapModel;

import java.io.Serializable;

public class PricingRoot implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Root RootObject;
 
 	public Root getRoot() {
 		return RootObject;
	}

 	public void setRoot(Root rootObject) {
 		this.RootObject = rootObject;
 	}
 	
}