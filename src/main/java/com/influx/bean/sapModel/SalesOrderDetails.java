
package com.influx.bean.sapModel;

import com.fasterxml.jackson.annotation.JsonProperty;


public class SalesOrderDetails {

    @JsonProperty("Address")
    private String address;
    @JsonProperty("Customer")
    private String customer;
    @JsonProperty("TermsPayment")
    private String termsPayment;
    @JsonProperty("PostalCode")
    private String postalCode;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("OrderStatus")
    private String orderStatus;
    @JsonProperty("TaxAmount")
    private String taxAmount;
    @JsonProperty("OrderType")
    private String orderType;
    @JsonProperty("Name1")
    private String name1;
    @JsonProperty("City1")
    private String city1;
    @JsonProperty("Amount")
    private String amount;
    @JsonProperty("Address2")
    private String address2;
    @JsonProperty("Region1")
    private String region1;
    @JsonProperty("Address3")
    private String address3;
    @JsonProperty("SALES_ORDER_ITEMSet")
    private SALESORDERITEMSet sALESORDERITEMSet;
    @JsonProperty("Text")
    private String text;
    @JsonProperty("Address1")
    private String address1;
    @JsonProperty("City")
    private String city;
    @JsonProperty("Flag")
    private String flag;
    @JsonProperty("Vbeln")
    private String vbeln;
    @JsonProperty("Customer1")
    private String customer1;
    @JsonProperty("NetValue")
    private String netValue;
    @JsonProperty("Region")
    private String region;
    @JsonProperty("DocCreated")
    private String docCreated;
    @JsonProperty("PostalCode1")
    private String postalCode1;
    @JsonProperty("CustomerRef")
    private String customerRef;
    @JsonProperty("CompanyCode")
    private String companyCode;

    @JsonProperty("Address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("Address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("Customer")
    public String getCustomer() {
        return customer;
    }

    @JsonProperty("Customer")
    public void setCustomer(String customer) {
        this.customer = customer;
    }

    @JsonProperty("TermsPayment")
    public String getTermsPayment() {
        return termsPayment;
    }

    @JsonProperty("TermsPayment")
    public void setTermsPayment(String termsPayment) {
        this.termsPayment = termsPayment;
    }

    @JsonProperty("PostalCode")
    public String getPostalCode() {
        return postalCode;
    }

    @JsonProperty("PostalCode")
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @JsonProperty("Name")
    public String getName() {
        return name;
    }

    @JsonProperty("Name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("OrderStatus")
    public String getOrderStatus() {
        return orderStatus;
    }

    @JsonProperty("OrderStatus")
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    @JsonProperty("TaxAmount")
    public String getTaxAmount() {
        return taxAmount;
    }

    @JsonProperty("TaxAmount")
    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    @JsonProperty("OrderType")
    public String getOrderType() {
        return orderType;
    }

    @JsonProperty("OrderType")
    public void setOrderType(String orderType) {
        this.orderType = orderType;
    }

    @JsonProperty("Name1")
    public String getName1() {
        return name1;
    }

    @JsonProperty("Name1")
    public void setName1(String name1) {
        this.name1 = name1;
    }

    @JsonProperty("City1")
    public String getCity1() {
        return city1;
    }

    @JsonProperty("City1")
    public void setCity1(String city1) {
        this.city1 = city1;
    }

    @JsonProperty("Amount")
    public String getAmount() {
        return amount;
    }

    @JsonProperty("Amount")
    public void setAmount(String amount) {
        this.amount = amount;
    }

    @JsonProperty("Address2")
    public String getAddress2() {
        return address2;
    }

    @JsonProperty("Address2")
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    @JsonProperty("Region1")
    public String getRegion1() {
        return region1;
    }

    @JsonProperty("Region1")
    public void setRegion1(String region1) {
        this.region1 = region1;
    }

    @JsonProperty("Address3")
    public String getAddress3() {
        return address3;
    }

    @JsonProperty("Address3")
    public void setAddress3(String address3) {
        this.address3 = address3;
    }

    @JsonProperty("SALES_ORDER_ITEMSet")
    public SALESORDERITEMSet getSALESORDERITEMSet() {
        return sALESORDERITEMSet;
    }

    @JsonProperty("SALES_ORDER_ITEMSet")
    public void setSALESORDERITEMSet(SALESORDERITEMSet sALESORDERITEMSet) {
        this.sALESORDERITEMSet = sALESORDERITEMSet;
    }

    @JsonProperty("Text")
    public String getText() {
        return text;
    }

    @JsonProperty("Text")
    public void setText(String text) {
        this.text = text;
    }

    @JsonProperty("Address1")
    public String getAddress1() {
        return address1;
    }

    @JsonProperty("Address1")
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @JsonProperty("City")
    public String getCity() {
        return city;
    }

    @JsonProperty("City")
    public void setCity(String city) {
        this.city = city;
    }

    @JsonProperty("Flag")
    public String getFlag() {
        return flag;
    }

    @JsonProperty("Flag")
    public void setFlag(String flag) {
        this.flag = flag;
    }

    @JsonProperty("Vbeln")
    public String getVbeln() {
        return vbeln;
    }

    @JsonProperty("Vbeln")
    public void setVbeln(String vbeln) {
        this.vbeln = vbeln;
    }

    @JsonProperty("Customer1")
    public String getCustomer1() {
        return customer1;
    }

    @JsonProperty("Customer1")
    public void setCustomer1(String customer1) {
        this.customer1 = customer1;
    }

    @JsonProperty("NetValue")
    public String getNetValue() {
        return netValue;
    }

    @JsonProperty("NetValue")
    public void setNetValue(String netValue) {
        this.netValue = netValue;
    }

    @JsonProperty("Region")
    public String getRegion() {
        return region;
    }

    @JsonProperty("Region")
    public void setRegion(String region) {
        this.region = region;
    }

    @JsonProperty("DocCreated")
    public String getDocCreated() {
        return docCreated;
    }

    @JsonProperty("DocCreated")
    public void setDocCreated(String docCreated) {
        this.docCreated = docCreated;
    }

    @JsonProperty("PostalCode1")
    public String getPostalCode1() {
        return postalCode1;
    }

    @JsonProperty("PostalCode1")
    public void setPostalCode1(String postalCode1) {
        this.postalCode1 = postalCode1;
    }

    @JsonProperty("CustomerRef")
    public String getCustomerRef() {
        return customerRef;
    }

    @JsonProperty("CustomerRef")
    public void setCustomerRef(String customerRef) {
        this.customerRef = customerRef;
    }

    @JsonProperty("CompanyCode")
    public String getCompanyCode() {
        return companyCode;
    }

    @JsonProperty("CompanyCode")
    public void setCompanyCode(String companyCode) {
        this.companyCode = companyCode;
    }



}
