package com.influx.bean.sapModel;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;

@JsonRootName("Record")
public class PricingRecord implements Serializable {

	private static final long serialVersionUID = 5926468583005150707L;

	
	public String errorMsg;

    private ProductPricing outputPrice;


	public String getErrorMsg() {
		return errorMsg;
	}

	@JsonProperty("ERRORMESSAGE")
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}

    @JsonProperty("outputPrice")
	public ProductPricing getOutputPrice() {
		return outputPrice;
	}

	@JsonProperty("OUTPUTPRICE")
	public void setOutputPrice(ProductPricing outputPrice) {
		this.outputPrice = outputPrice;
	}

}