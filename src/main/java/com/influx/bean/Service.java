package com.influx.bean;

/**
 * Created by chris on 11/19/14.
 */
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Service
{
    private String name;

    public Service() {}
    public Service(String name) { this.name = name; }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

}
