package com.influx.bean;

import java.math.BigDecimal;

/**
 * Created by chris on 2/5/15.
 */
public class CatalogItem
{
    private String prod;
    private String description;
    private String error;
    private int netAvailCompany;
    private BigDecimal listPrice;
    private char status;

    public CatalogItem() {}

    public CatalogItem(String prod)
    {
        this.setProd(prod);
    }

    public String getProd() {
        return prod;
    }

    public void setProd(String prod) {
        this.prod = prod;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public int getNetAvailCompany() {
        return netAvailCompany;
    }

    public void setNetAvailCompany(int netAvailCompany) {
        this.netAvailCompany = netAvailCompany;
    }

    public BigDecimal getListPrice() {
        return listPrice;
    }

    public void setListPrice(BigDecimal listPrice) {
        this.listPrice = listPrice;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }
}
