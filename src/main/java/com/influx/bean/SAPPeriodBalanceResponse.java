package com.influx.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SAPPeriodBalanceResponse implements Serializable {

	private static final long serialVersionUID = 5926468583005150707L;

    @JsonProperty("Response") 
    public BalanceResponse response;

	public BalanceResponse getResponse() {
		return response;
	}

	public void setResponse(BalanceResponse response) {
		this.response = response;
	}

    
    

}
