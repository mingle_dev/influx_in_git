package com.influx.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class InsiteOrder {
	
    private List<InsiteCartLine> products;
	private String email;
	private String password;
	private String wareHouse;
	private String shipVia;
	private String shipDate;	
	private String poNumber;
	private String reference;
	private String accessToken;
	private String status;
	private String orderStatus;
	private String gateWayStatus;
	private String webOrder;
	private String orderNumber;
    private String custNo;
    private String shipTo;
	
	public List<InsiteCartLine> getProducts() {
		return products;
	}
	public void setProducts(List<InsiteCartLine> products) {
		this.products = products;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getWareHouse() {
		return wareHouse;
	}
	public void setWareHouse(String wareHouse) {
		this.wareHouse = wareHouse;
	}
	public String getShipVia() {
		return shipVia;
	}
	public void setShipVia(String shipVia) {
		this.shipVia = shipVia;
	}
	public String getShipDate() {
		return shipDate;
	}
	public void setShipDate(String shipDate) {
		this.shipDate = shipDate;
	}
	public String getPoNumber() {
		return poNumber;
	}
	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}
	public String getReference() {
		return reference;
	}
	public void setReference(String reference) {
		this.reference = reference;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public String getOrderStatus() {
		return orderStatus;
	}
	public void setOrderStatus(String orderStatus) {
		this.orderStatus = orderStatus;
	}
	public String getCustNo() {
		return custNo;
	}
	public void setCustNo(String custNo) {
		this.custNo = custNo;
	}
	public String getShipTo() {
		return shipTo;
	}
	public void setShipTo(String shipTo) {
		this.shipTo = shipTo;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getGateWayStatus() {
		return gateWayStatus;
	}
	public void setGateWayStatus(String gateWayStatus) {
		this.gateWayStatus = gateWayStatus;
	}
	public String getWebOrder() {
		return webOrder;
	}
	public void setWebOrder(String webOrder) {
		this.webOrder = webOrder;
	}
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

}
