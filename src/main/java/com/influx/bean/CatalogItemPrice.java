package com.influx.bean;

import java.math.BigDecimal;


public class CatalogItemPrice extends CatalogItem
{
    private String whse;
    private int netAvailWhse;
    private BigDecimal price;

    public CatalogItemPrice() {}

    public CatalogItemPrice(String prod) {
        this.setProd(prod);
    }

    public CatalogItemPrice(String prod, String whse, int netAvailWhse, BigDecimal price)
    {
        this.setProd(prod);
        this.setWhse(whse);
        this.setNetAvailWhse(netAvailWhse);
        this.setPrice(price);
    }

    public CatalogItemPrice(String prod, String whse, int netAvailWhse, BigDecimal price, String error)
    {
        this.setProd(prod);
        this.setWhse(whse);
        this.setNetAvailWhse(netAvailWhse);
        this.setPrice(price);
        this.setError(error);
    }

    public String getWhse() {
        return whse;
    }

    public void setWhse(String whse) {
        this.whse = whse;
    }

    public int getNetAvailWhse() {
        return netAvailWhse;
    }

    public void setNetAvailWhse(int netAvailWhse) {
        this.netAvailWhse = netAvailWhse;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}