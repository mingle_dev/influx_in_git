package com.influx.bean;

import java.io.Serializable;
import java.util.List;


/**
 *
 * @author Nara
 */
public class LineItems implements Serializable
{
   
	private List<LineItem> lineItem;

	/**
	 * @return the lineItems
	 */
	public List<LineItem> getLineItem() {
		return lineItem;
	}

	/**
	 * @param lineItems the lineItems to set
	 */
	public void setLineItem(List<LineItem> lineItem) {
		this.lineItem = lineItem;
	}
    
	
    
}